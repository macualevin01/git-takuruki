@extends('layouts.library')
{{-- Web site Title --}}
@section('title')
  Dashboard  @parent
@stop
@section('styles')
@endsection
@section('content')
 @include('layouts.sidebar')
 <div class="page-wrapper" style="background:#F5FFF9 !important;">
	@include('layouts.navbar')
		<div class="page-content">
			<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><h5>Dashboard</h5></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><h5>Cards</h5></li>
                    </ol>
                </div>
                <div class="d-flex align-items-center flex-wrap text-nowrap">
                   <a href="{{ url('card/create') }}" class="btn btn-outline-primary"> 
                   	<i class="link-icon" data-feather="plus"></i>
            		<span class="link-title">Add Card</span>
            		</a>
                </div>
            </div>

            <div class="row" align="center">
            	<div class="col-md-12 grid-margin stretch-card">
            		<div class="card">
            			<div class="card-body">
            				<div class="table-responsive">
	            				<table id="table" class="table table-striped table-hover"  width="100%">
	            					<thead>
		            					<tr>
		            						<th class="text-muted">Date</th>
		            						<th class="text-muted">Card Name</th>
		            						<th class="text-muted">Description</th>
		            						<th class="text-muted">Action</th>
		            					</tr>
	            					</thead>
	            				</table>
            				</div>
            			</div>
            		</div>
	            </div>
        	</div> 
        </div>
	@include('layouts.footer')
 </div>
@endsection
@section('scripts')
<script type="text/javascript">
	// var oTable ;
    $(document).ready(function(){
    	$('#table').DataTable( {
            "ajax": {
                "type" : "GET",
                "url" : "{{ URL::to('add_card/data')}}",
                "dataSrc": function ( json ) 
                {
                    return json.data;
                }       
            },
            "bSort" : false,
            "processing": true,
            "language": {
                "loadingRecords": '&nbsp;',
                "processing": 'Loading...'
            },
            "columns": [
                { data: 'created_at' },
                { data: 'card_name' },
                { data: 'description'},
                { data: 'actions', name: 'actions', orderable:false, searchable:false }
            ],
            "fnDrawCallback": function ( oSettings ) {
            },
        });
	});
</script>
@endsection


