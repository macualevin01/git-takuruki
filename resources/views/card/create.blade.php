@extends('layouts.library')
{{-- Web site Title --}}
@section('title')
  Dashboard  @parent
@stop
@section('styles')
@endsection
@section('content')
 @include('layouts.sidebar')
 <div class="page-wrapper" style="background:#F5FFF9 !important;">
	@include('layouts.navbar')
		<div class="page-content">
			<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><h5>Dashboard</h5></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('add_card') }}"><h5>Cards</h5></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><h5>Add Card</h5></li>
                    </ol>
                </div>
            </div>

            <div class="row" align="center">
	            <div class="col-md-3 grid-margin stretch-card">
	            </div>
              	<div class="col-md-6 grid-margin stretch-card">
	                <div class="card">
	                    <div class="card-body">
	                        <form class="forms-sample" method="POST" action="{{ url('card/create') }}">
                                {{ csrf_field() }}
								<div class="row mb-3" align="center">
									<label class="form-label text-muted">Card Name</label>
									<input type="text" class="form-control form-control-lg" id="card_name" name="card_name" placeholder="" required>
								</div>
								<div class="row mb-3" align="center">
									<label class="form-label text-muted">Description</label>
									<textarea class="form-control form-control-lg" id="description" name="description" placeholder="Type here.."></textarea>
								</div>
								<button type="submit" class="btn btn-primary col-md-6 btn-lg">Save </button>
	                        </form>
	                    </div> 
	                </div>
            	</div>
        	</div> 
        </div>
	@include('layouts.footer')
 </div>
@endsection
@section('scripts')
    @if(session()->has('success'))
        <script type="text/javascript">
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });
            Toast.fire({
              icon: 'success',
              title: 'Success!',
              text:'You have been successfully added new card.'
            });
        </script>
    @endif
    @if(session()->has('errors'))
        <script type="text/javascript">
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });
            Toast.fire({
              icon: 'warning',
              title: 'Error!',
              text:'Some field is missing.'
            });
        </script>
    @endif
@endsection


