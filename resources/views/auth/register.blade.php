@extends('layouts.library')

@section('content')
    <div class="page-wrapper full-page" style="background:#F5FFF9;">
        <div class="page-content d-flex align-items-center justify-content-center">
            <div class="row w-100 mx-0 auth-page">
                <div class="col-md-2 col-xl-3 mx-auto">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-12 ps-md-0">
                                <div class="auth-form-wrapper px-4 py-4">
                                    <h4 class="text-muted fw-normal mb-4">Register</h4>
                                    <form class="forms-sample" method="POST" action="{{ route('register') }}">
                                                {{ csrf_field() }}
                                                <div class="mb-3{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label for="name" class="form-label">Name</label>
                                                    <input type="text" class="form-control" id="name" name="name" value="" placeholder="Name" required autofocus>
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="userEmail" class="form-label">Email address</label>
                                                    <input type="email" class="form-control" id="userEmail" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                        
                                                <div class="mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label for="userPassword" class="form-label">Password</label>
                                                    <input type="password" class="form-control" id="userPassword" name="password" autocomplete="current-password" placeholder="Password" required autofocus>
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
        
                                                <div class="mb-3{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                     <label for="password-confirm" class="control-label">Confirm Password</label>
                                                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                    
                                            <!-- <a href="{{ route('login') }}" type="submit" class="btn btn-primary col-md-5  me-2 mb-2 mb-md-0 text-white">Back to Login</a> -->
                                            <button type="submit" class="btn btn-success col-md-6 btn-icon-text mb-2 mb-md-0">
                                                Register
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--     <div class="page-wrapper full-page" style="background:#F5FFF9;">
        <div class="page-content d-flex align-items-center justify-content-center">
            <div class="row col-md-5">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="wizard">
                              <h2></h2>
                              <section st>
                                <h4>First Step</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut nulla nunc. Maecenas arcu sem, hendrerit a tempor quis, 
                                    sagittis accumsan tellus. In hac habitasse platea dictumst. Donec a semper dui. Nunc eget quam libero. Nam at felis metus. 
                                    Nam tellus dolor, tristique ac tempus nec, iaculis quis nisi.</p>
                              </section>
              
                              <h2></h2>
                              <section>
                                <h4>Second Step</h4>
                                <p>Donec mi sapien, hendrerit nec egestas a, rutrum vitae dolor. Nullam venenatis diam ac ligula elementum pellentesque. 
                                    In lobortis sollicitudin felis non eleifend. Morbi tristique tellus est, sed tempor elit. Morbi varius, nulla quis condimentum 
                                    dictum, nisi elit condimentum magna, nec venenatis urna quam in nisi. Integer hendrerit sapien a diam adipiscing consectetur. 
                                    In euismod augue ullamcorper leo dignissim quis elementum arcu porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                    Vestibulum leo velit, blandit ac tempor nec, ultrices id diam. Donec metus lacus, rhoncus sagittis iaculis nec, malesuada a diam. 
                                    Donec non pulvinar urna. Aliquam id velit lacus.</p>
                              </section>
              
                              <h2></h2>
                              <section>
                                <h4>Third Step</h4>
                                <p>Morbi ornare tellus at elit ultrices id dignissim lorem elementum. Sed eget nisl at justo condimentum dapibus. Fusce eros justo, 
                                    pellentesque non euismod ac, rutrum sed quam. Ut non mi tortor. Vestibulum eleifend varius ullamcorper. Aliquam erat volutpat. 
                                    Donec diam massa, porta vel dictum sit amet, iaculis ac massa. Sed elementum dui commodo lectus sollicitudin in auctor mauris 
                                    venenatis.</p>
                              </section>
              
                              <h2></h2>
                              <section>
                                <h4>Fourth Step</h4>
                                <p>Quisque at sem turpis, id sagittis diam. Suspendisse malesuada eros posuere mauris vehicula vulputate. Aliquam sed sem tortor. 
                                    Quisque sed felis ut mauris feugiat iaculis nec ac lectus. Sed consequat vestibulum purus, imperdiet varius est pellentesque vitae. 
                                    Suspendisse consequat cursus eros, vitae tempus enim euismod non. Nullam ut commodo tortor.</p>
                              </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
@endsection
