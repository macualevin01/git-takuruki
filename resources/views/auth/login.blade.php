@extends('layouts.library')
@section('title')
  Login  @parent
@stop
@section('content')
    <div class="page-wrapper full-page" style="background:#F5FFF9;">
        <div class="page-content d-flex align-items-center justify-content-center">
            <div class="row w-100 mx-0 auth-page">
                <div class="col-md-2 col-xl-3 mx-auto">
                    <div class="card">
                        <div class="row">
                            <!-- login form -->
                            <div class="col-md-12 ps-md-0">
                                <div class="auth-form-wrapper px-4 py-5">
                                    <img src="{{ asset('assets/images/dep_ed.png') }}" width="100px">
                                    <!-- <a href="#" class="noble-ui-logo d-block mb-2">Takuruki's <span>Teahcers Class Record</span></a> -->
                                    <h5 class="text-muted fw-normal mb-4">Welcome back! Log in to your account.</h5>
                                    <form class="forms-sample" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                        <div class="mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="userEmail" class="form-label">Email address</label>
                                            <input type="email" class="form-control" id="userEmail" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="userPassword" class="form-label">Password</label>
                                            <input type="password" class="form-control" id="userPassword" name="password" autocomplete="current-password" placeholder="Password" required autofocus>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-check mb-2">
                                            <label class="form-check-label" for="authCheck">
                                                <input type="checkbox" class="form-check-input" id="authCheck"> Remember me
                                            </label>
                                        </div>
                                        <div class="row mb-3" align="center">
                                            <div class="col">
                                                 <button type="submit" class="btn btn-primary me-2 mb-2 mb-md-0 col-md-12 text-white">Login</button>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ route('register') }}" class="btn btn-primary me-2 mb-2 mb-md-0 col-md-12 text-white">Singup</a>
                                            </div>
                                        </div>
                                        <a href="{{ route('password.request') }}" class="d-block text_hover text-muted"> Forgot Your Password? </a>
                                    </form>
                                </div>
                            </div>
                            <!-- exit -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
