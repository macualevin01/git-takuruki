@extends('layouts.library')
{{-- Web site Title --}}
@section('title')
  Dashboard  @parent
@stop
@section('styles')
@endsection
@section('content')
 @include('layouts.sidebar')
 <div class="page-wrapper" style="background:#F5FFF9 !important;">
	@include('layouts.navbar')
		<div class="page-content">
			<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
				<div>
				 <h4 class="mb-3 mb-md-0">Welcome {{ Auth::user()->name }} </h4>
				</div>
				<div class="d-flex align-items-center flex-wrap text-nowrap">
					<div class="input-group date datepicker dashboard-date mr-2 mb-2 mb-md-0 d-md-none d-xl-flex" id="dashboardDate">
						<span class="input-group-addon bg-transparent"><i data-feather="calendar" class=" text-primary"></i></span>
						<input type="text" class="form-control">
					</div>
				</div>
			</div>
		<div class="row">
		  <div class="col-12 col-xl-12 stretch-card">
			<div class="row flex-grow">
			  <div class="col-md-3 grid-margin stretch-card">
				<div class="card">
				  <div class="card-body">
					<div class="d-flex justify-content-between align-items-baseline">
					  <h6 class="card-title mb-0">Users</h6>
					  <div class="dropdown mb-2">
						<button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm mr-2"></i> <span class="">Print</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm mr-2"></i> <span class="">Download</span></a>
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-12 col-md-12 col-xl-12">
						<h3 class="mb-2"> <b><i class="link-icon" data-feather="users"></i></b> 0.00</h3>
					  </div>
					  <div class="col-12 col-md-12 col-xl-12">
						<div id="userschart" class="mt-md-3 mt-xl-0"></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="col-md-3 grid-margin stretch-card">
				<div class="card">
				  <div class="card-body">
					<div class="d-flex justify-content-between align-items-baseline">
					  <h6 class="card-title mb-0">Students</h6>
					  <div class="dropdown mb-2">
						<button class="btn p-0" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm mr-2"></i> <span class="">Print</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm mr-2"></i> <span class="">Download</span></a>
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-12 col-md-12 col-xl-12">
						<h3 class="mb-2"> <b><i class="link-icon" data-feather="plus"></i><i class="link-icon" data-feather="users"></i></b> 0.00 </h3>
					  </div>
					  <div class="col-12 col-md-12 col-xl-12">
						<div id="daily-collection" class="mt-md-3 mt-xl-0"></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="col-md-3 grid-margin stretch-card">
				<div class="card">
				  <div class="card-body">
					<div class="d-flex justify-content-between align-items-baseline">
					  <h6 class="card-title mb-0">Dep-ED Reports</h6>
					  <div class="dropdown mb-2">
						<button class="btn p-0" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm mr-2"></i> <span class="">Print</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm mr-2"></i> <span class="">Download</span></a>
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-12 col-md-12 col-xl-12">
						<h3 class="mb-2"><b><i class="link-icon" data-feather="file"></i></b>0.00</h3>
					  </div>
					  <div class="col-12 col-md-12 col-xl-12">
						<div id="over-all-colection" class="mt-md-3 mt-xl-0"></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="col-md-3 grid-margin stretch-card">
				<div class="card">
				  <div class="card-body">
					<div class="d-flex justify-content-between align-items-baseline">
					  <h6 class="card-title mb-0">Honors Report</h6>
					  <div class="dropdown mb-2">
						<button class="btn p-0" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm mr-2"></i> <span class="">Print</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm mr-2"></i> <span class="">Download</span></a>
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-12 col-md-12 col-xl-12">
						<h3 class="mb-2"><b><i class="link-icon" data-feather="layers"></i></b> 0.00</h3>
					  </div>
					  <div class="col-12 col-md-12 col-xl-12">
						<div id="ongoing-colection" class="mt-md-3 mt-xl-0"></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-xl-12 grid-margin stretch-card">
				<div class="card">
				  <div class="card-body">
					<div class="d-flex justify-content-between align-items-baseline mb-2">
					  <h6 class="card-title mb-0">Students </h6>
					  <div class="dropdown mb-2">
						<button class="btn p-0" type="button" id="dropdownMenuButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton4">
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm mr-2"></i> <span class="">Print</span></a>
						  <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm mr-2"></i> <span class="">Download</span></a>
						</div>
					  </div>
					</div>
					<p class="text-muted mb-4"></p>
					<div class="monthly-sales-chart-wrapper">
					  <canvas id="monthly-collection"></canvas>
					</div>
				  </div> 
				</div>
			</div>
		</div> 
	</div>
	@include('layouts.footer')
 </div>
@endsection


