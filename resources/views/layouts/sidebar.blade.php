<script src="{{ asset('library/js/spinner.js')}}"></script>
<nav class="sidebar">
    <div class="sidebar-header">
      <a href="#" class="sidebar-brand">
        Takuruki<span>Club</span>
      </a>
      <div class="sidebar-toggler not-active">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <div class="sidebar-body">
      <ul class="nav">
        <li class="nav-item nav-category">Main</li>
        <li class="nav-item {{ request()->is('*dashboard*') ? 'active' : '' }}">
          <a href="{{ url('dashboard') }}" class="nav-link">
            <i class="link-icon" data-feather="box"></i>
            <span class="link-title">Dashboard</span>
          </a>
        </li>

        <!-- <li class="nav-item {{ request()->is('*class_record*') ? 'active' : '' }}">
          <a href="{{ url('class_record') }}" class="nav-link">
            <i class="link-icon" data-feather="layers"></i>
            <span class="link-title">Class Record</span>
          </a>
        </li>
 -->
        <li class="nav-item {{ request()->is('*events*') ? 'active' : '' }}">
          <a href="{{ url('events') }}" class="nav-link">
            <i class="link-icon" data-feather="calendar"></i>
            <span class="link-title">Events</span>
          </a>
        </li>
        <li class="nav-item {{ request()->is('*add_card*') ? 'active' : '' }}">
          <a href="{{ url('add_card') }}" class="nav-link">
            <i class="link-icon" data-feather="list"></i>
            <span class="link-title">Cards</span>
          </a>
        </li>
      <!--   <li class="nav-item {{ request()->is('*student*') ? 'active' : '' }}">
          <a href="{{ url('student') }}" class="nav-link">
            <i class="link-icon" data-feather="list"></i>
            <span class="link-title">Student List</span>
          </a>
        </li> -->

        <li class="nav-item nav-category">Manage</li>
        <li class="nav-item {{ request()->is('*chart_of_account*') ? 'active' : '' }}">
           <a href="{{ url('chart_of_account/create') }}" class="nav-link {{ request()->is('*chart_of_account/create*') ? 'active' : '' }}">  
            <i class="link-icon" data-feather="settings"></i>
            <span class="link-title">Settings</span></a>
        </li>
        <li class="nav-item {{ request()->is('*chart_of_account*') ? 'active' : '' }}">
           <a href="{{ url('chart_of_account/create') }}" class="nav-link {{ request()->is('*chart_of_account/create*') ? 'active' : '' }}">  
            <i class="link-icon" data-feather="bell"></i>
            <span class="link-title">Notification</span></a>
        </li>
        <li class="nav-item nav-category">Reports</li>
        <li class="nav-item {{ request()->is('*general_ledger*') ? 'active' : '' }}">
          <a href="{{ url('general_ledger') }}" class="nav-link">
            <i class="link-icon" data-feather="users"></i>
            <span class="link-title">User List</span>
          </a>
        </li>
      </ul>
    </div>
</nav>
<nav class="settings-sidebar">
    <div class="sidebar-body">
      <a href="#" class="settings-sidebar-toggler">
        <i data-feather="settings"></i>
      </a>
      <h6 class="text-muted">Sidebar:</h6>
      <div class="form-group border-bottom">
        <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarLight" value="sidebar-light" {{ \Auth::user()->theme == 'sidebar-light' ? 'checked' : '' }}>
            Light
          </label>
        </div>
        <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarDark" value="sidebar-dark" {{ \Auth::user()->theme == 'sidebar-dark' ? 'checked' : '' }}>
            Dark
          </label>
        </div>
      </div>
    </div>
 </nav>  