  @if (Auth::guest()) 
  @else
    <nav class="navbar">
    <a href="#" class="sidebar-toggler">
          <i data-feather="menu"></i>
    </a>
    <div class="navbar-content">
      <form class="search-form">
      </form>
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flag-icon flag-icon-ph mt-1" title="ph"></i> <span class="font-weight-medium ml-1 mr-1">Philippines</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="languageDropdown">
            <a href="javascript:;" class="dropdown-item py-2"><i class="flag-icon flag-icon-ph" title="ph" id="ph"></i> <span class="ml-1"> Philippines </span></a>
            <a href="javascript:;" class="dropdown-item py-2"><i class="flag-icon flag-icon-us" title="us" id="us"></i> <span class="ml-1"> English </span></a>
          </div>
        </li>
        <li class="nav-item dropdown nav-messages">
          <a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i data-feather="mail"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="messageDropdown">
            <div class="dropdown-header d-flex align-items-center justify-content-between">
              <p class="mb-0 font-weight-medium">0 New Messages</p>
              <a href="javascript:;" class="text-muted">Clear all</a>
            </div>
            <div class="dropdown-body">
              <a href="javascript:;" class="dropdown-item">
                <div class="content">
                  <div class="d-flex justify-content-between align-items-center">
                    <p>No Message found Yet</p>
                  </div>  
                </div>
              </a>
            </div>
            <div class="dropdown-footer d-flex align-items-center justify-content-center">
              <a href="javascript:;">View all</a>
            </div>
          </div>
        </li>
        <li class="nav-item dropdown nav-notifications">
          <a class="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i data-feather="bell"></i>
            <div class="indicator">
            </div>
          </a>
          <div class="dropdown-menu" aria-labelledby="notificationDropdown">
            <div class="dropdown-header d-flex align-items-center justify-content-between">
              <p class="mb-0 font-weight-medium">0 New Notifications</p>
              <a href="javascript:;" class="text-muted">Clear all</a>
            </div>
            <div class="dropdown-body">
              <a href="javascript:;" class="dropdown-item">
                <div class="content">
                  <p class="sub-text text-muted">No Notification Found Yet</p>
                </div>
              </a>
            </div>
            <div class="dropdown-footer d-flex align-items-center justify-content-center">
              <a href="javascript:;">View all</a>
            </div>
          </div>
        </li>
        <li class="nav-item dropdown nav-profile">
          <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="{{ asset('assets/images/default-avatar.png') }}" alt="profile">
          </a>
          <div class="dropdown-menu" aria-labelledby="profileDropdown">
            <div class="dropdown-header d-flex flex-column align-items-center">
              <div class="figure mb-3">
                  <img src="{{ asset('assets/images/default-avatar.png') }}" alt="profile">
              </div>
              <div class="info text-center">
                <p class="name font-weight-bold mb-0"> {{ Auth::user()->name }} </p>
                <p class="email text-muted mb-3">{{ Auth::user()->email }}</p>
              </div>
            </div>
            <div class="dropdown-body">
              <ul class="profile-nav p-0 pt-3">
                <li class="nav-item">
                  <a href="" class="nav-link">
                    <i data-feather="user"></i>
                    <span>Profile</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link">
                    <i data-feather="log-out"></i>
                    <span>Log Out</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </nav>   
  @endif