<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <link rel="shortcut icon" href="{{ asset('assets/landing_page/images/kaikeisoft_logo.png')}}">
    <title>Takuruki :: @yield('title')</title>
    <!-- Styles -->

    <!-- plugin css -->
      <link href="{{ asset('library/fonts/feather-font/css/iconfont.css')}}" rel="stylesheet" />
      <link href="{{ asset('library/plugins/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet" />
      <link href="{{ asset('library/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" />
       <!-- end plugin css -->
       <link rel="stylesheet" href="{{ asset('library/css/jquery.steps.css')}}">
       <!-- Plugin css for this page MDI -->
       <link rel="stylesheet" href="{{ asset('library/plugins/%40mdi/css/materialdesignicons.min.css') }}"> 
       <!-- End plugin css for this page -->
      <link href="{{ asset('library/plugins/select2/select2.min.css') }}" rel="stylesheet" />
      <link href="{{ asset('library/plugins/jquery-tags-input/jquery.tagsinput.min.css') }}" rel="stylesheet" />

        <link href="{{ asset('library/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('library/plugins/datatables-net/datatables.min.css') }}" />
        <link href="{{ asset('assets/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet">

      <link href="{{ asset('library/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
      <!-- common css -->
      <link href="{{ asset('library/plugins/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" />
      <link href="{{ asset('library/css/homepage.css') }}" rel="stylesheet" />

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- end common css -->
      <style type="text/css">
        i{

          font-size:20px !important;
          cursor: pointer !important;
        }
      </style>
      @yield('styles')
</head>
<body>
    <div class="main-wrapper" id="app" style="background:#F5FFF9 !important;">
        @yield('content')
    </div>
      <!-- base js -->
    <script src="{{ asset('library/js/main.js') }}"></script>
    <script src="{{ asset('library/plugins/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('library/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!-- end base js -->
    <script src="{{ asset('library/js/jquery.steps.min.js') }}"></script>
    <!-- plugin js -->
    <script src="{{ asset('library/plugins/chartjs/Chart.min.js') }}"></script>
    <script src="{{ asset('library/plugins/jquery.flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('library/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('library/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('library/plugins/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('library/plugins/progressbar-js/progressbar.min.js') }}"></script>
    <!-- end plugin js -->
    <!-- plugin js -->
    <script src="{{ asset('library/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('library/plugins/promise-polyfill/polyfill.min.js') }}"></script>
    <!-- end plugin js -->
   <!-- datatables js-->
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{asset('assets/site/js/datatables.fnReloadAjax.js')}}"></script>
    <!-- common js -->
    <script src="{{ asset('library/js/main-template.js') }}"></script>
    <!-- end common js -->
    <script src="{{ asset('library/js/dashboard.js') }}"></script>
    <script src="{{ asset('library/js/datepicker.js') }}"></script>
    <script src="{{ asset('library/js/wizard.js') }}"></script>

     @yield('scripts')
</body>
</html>
