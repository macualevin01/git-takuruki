<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();
Route::get('/dashboard', 'DashboardMainController@index')->name('index');

// card
Route::get('add_card', 'CardMainConteroller@index')->name('index');
Route::get('card/create', 'CardMainConteroller@create');
Route::post('card/create', 'CardMainConteroller@store');
Route::get('add_card/data', 'CardMainConteroller@data');

Route::get('events', 'EventsMainController@index')->name('index');
