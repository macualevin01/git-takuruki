var hot;

$(document).ready(function () {
    inputGradeHandsonTable();
    inputDeportmentHandsonTable();
    inputCoCurricularHandsonTable();
    sf10AttendanceBulkHandsonTable();
});



function inputGradeHandsonTable() {

    $('#inputgrademodal').on('shown.bs.modal', function (e) {
        $('#input_grade_excel').empty();
        // $(".htCore tbody tr").remove();

        var button =  $(e.relatedTarget)
        var Id = button.data('id')
        var LastName = button.data('last_name')
        var FirstName = button.data('first_name')
        var MiddleName = button.data('middle_name')
        var TermId = button.data('term_id')
        var TermName = button.data('term_name')
        var Level = button.data('level')
        var ClassificationLevelId = button.data('level_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SemesterLevel = button.data('semester_level')
        var EnrollmentId = button.data('enrollment_id')


        var modal = $(this)
        $('.input_grade').text('Input Grades of ' +LastName+', '+FirstName+' '+MiddleName+' ('+TermName+' - ' +SemesterLevel+' '+Level+') ')
        $('#ig_student_id').val(Id)
        $('#ig_term_id').val(TermId)
        $('#ig_classification_level_id').val(ClassificationLevelId)
        $('#ig_semester_level_id').val(SemesterLevelId)
        $('#ig_enrollment_id').val(EnrollmentId)
        $('#ig_term_name').val(TermName)

        $.ajax({
            url:"../grade_tracking/dataJsonInputGrade",
            type:'get',
            data:
                {  
                    'student_id' : Id,
                    'classification_level_id' : ClassificationLevelId,
                    'term_id' : TermId,
                    'semester_level_id' : SemesterLevelId,
                    'enrollment_id' : EnrollmentId,
                    'term_name' : TermName
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                InputGradeArray = data;
            } 

        });

        var data = InputGradeArray[0];
        var class_input_id = InputGradeArray[1];

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }



        var container = document.getElementById("input_grade_excel");
        hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {

                var perfect_grade = 100;
                var grade_entry = parseInt(changes[0][3]);
                if(grade_entry > perfect_grade ){
                    sweetAlert("Oops...", "Grade must be lesser than or equal to 100!", "error");
                    return false;
                }

            },
            afterChange: function (changes, source) {

                if(changes != null){
                    var input_grade_row = changes[0][0];
                    var input_grade_column = changes[0][1];
                    var class_id = class_input_id[input_grade_row][input_grade_column];
                    var grade = changes[0][3];
                        if(grade == ""){

                        }
                        $.ajax({
                            url: "../grade_tracking/postUpdateInputGrade",
                            data: {
                                'id': class_id, 
                                'grade': grade,
                                '_token': $('input[name=_token]').val(),
                            }, 
                            dataType: 'json',
                            type: 'POST',
                            async:false
                        });
                    }
            },
            className: "htCenter htMiddle",
            height: 450,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            cells : function(row, col, prop, changes){
                var cellProperties = {};
                // if(row != 0 || row === 0) {
                //     cellProperties.editor = false;
                // }
                // if (row === 0 || col === 0) {
                //     cellProperties.renderer = firstRowRenderer; // uses function directly
                // }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                // var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                // var input_grade_column = selection[1];
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 )){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
                
            }
        });
    });
}
function inputDeportmentHandsonTable() {

    $('#inputgrademodal').on('shown.bs.modal', function (e) {
        $('#input_deportment_excel').empty();
        // $(".htCore tbody tr").remove();

        var button =  $(e.relatedTarget)
        var Id = button.data('id')
        var LastName = button.data('last_name')
        var FirstName = button.data('first_name')
        var MiddleName = button.data('middle_name')
        var TermId = button.data('term_id')
        var TermName = button.data('term_name')
        var Level = button.data('level')
        var ClassificationLevelId = button.data('level_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SemesterLevel = button.data('semester_level')


        var modal = $(this)
        $('.input_grade').text('Input Grades of ' +LastName+', '+FirstName+' '+MiddleName+' ('+TermName+' - ' +SemesterLevel+' '+Level+') ')
        $('#ig_student_id').val(Id)
        $('#ig_term_id').val(TermId)
        $('#ig_classification_level_id').val(ClassificationLevelId)
        $('#ig_semester_level_id').val(SemesterLevelId)
        $('#ig_term_name').val(TermName)

        $.ajax({
            url:"../grade_tracking/dataJsonInputDeportmentGrade",
            type:'get',
            data:
                {  
                    'student_id' : Id,
                    'classification_level_id' : ClassificationLevelId,
                    'term_id' : TermId,
                    'semester_level_id' : SemesterLevelId,
                    'term_name' : TermName,
                    '_token': $('input[name=_token]').val()
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                InputDeportmentArray = data;
            } 

        });

        var data = InputDeportmentArray[0];
        var class_input_id = InputDeportmentArray[1];
        var grading_period_input_id = InputDeportmentArray[2];

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }



        var container = document.getElementById("input_deportment_excel");
        hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {

                var perfect_grade = 100;
                var grade_entry = parseInt(changes[0][3]);
                if(grade_entry > perfect_grade ){
                    sweetAlert("Oops...", "Grade must be lesser than or equal to 100!", "error");
                    return false;
                }

            },
            afterChange: function (changes, source) {

                if(changes != null){
                    var input_grade_row = changes[0][0];
                    var input_grade_column = changes[0][1];
                    var grading_period_column = changes[0][2];
                    var grade_level_deportment_id = class_input_id[input_grade_row][input_grade_column];
                    var grading_period_column_id = grading_period_input_id[input_grade_row][input_grade_column]; 

                    var grade = changes[0][3];
                        if(grade == ""){

                        }
                        $.ajax({
                            url: "../grade_tracking/postUpdateInputGradeDeportment",
                            data: {
                                'id': grade_level_deportment_id, 
                                'grade': grade,
                                'student_id': $('#ig_student_id').val(),
                                'term_id': $('#ig_term_id').val(),
                                'grading_period_id': grading_period_column_id,
                                '_token': $('input[name=_token]').val(),
                            }, 
                            dataType: 'json',
                            type: 'POST',
                            async:false
                        });
                    }
            },
            className: "htCenter htMiddle",
            height: 450,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            cells : function(row, col, prop, changes){
                var cellProperties = {};
                // if(row != 0 || row === 0) {
                //     cellProperties.editor = false;
                // }
                // if (row === 0 || col === 0) {
                //     cellProperties.renderer = firstRowRenderer; // uses function directly
                // }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                // var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                // var input_grade_column = selection[1];
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 )){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
                
            }
        });
    });
}

function inputCoCurricularHandsonTable() {

    $('#inputgrademodal').on('shown.bs.modal', function (e) {
        $('#input_co_curriculum_excel').empty();
        // $(".htCore tbody tr").remove();

        var button =  $(e.relatedTarget)
        var Id = button.data('id')
        var LastName = button.data('last_name')
        var FirstName = button.data('first_name')
        var MiddleName = button.data('middle_name')
        var TermId = button.data('term_id')
        var TermName = button.data('term_name')
        var Level = button.data('level')
        var ClassificationLevelId = button.data('level_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SemesterLevel = button.data('semester_level')


        var modal = $(this)
        $('.input_grade').text('Input Grades of ' +LastName+', '+FirstName+' '+MiddleName+' ('+TermName+' - ' +SemesterLevel+' '+Level+') ')
        $('#ig_student_id').val(Id)
        $('#ig_term_id').val(TermId)
        $('#ig_classification_level_id').val(ClassificationLevelId)
        $('#ig_semester_level_id').val(SemesterLevelId)
        $('#ig_term_name').val(TermName)

        $.ajax({
            url:"../grade_tracking/dataJsonInputCoCurricularGrade",
            type:'get',
            data:
                {  
                    'student_id' : Id,
                    'classification_level_id' : ClassificationLevelId,
                    'term_id' : TermId,
                    'semester_level_id' : SemesterLevelId,
                    'term_name' : TermName,
                    '_token': $('input[name=_token]').val()
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                InputCoCurricularArray = data;
            } 

        });

        var data = InputCoCurricularArray[0];
        var class_input_id = InputCoCurricularArray[1];

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }



        var container = document.getElementById("input_co_curriculum_excel");
        hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {

                var perfect_grade = 100;
                var grade_entry = parseInt(changes[0][3]);
                if(grade_entry > perfect_grade ){
                    sweetAlert("Oops...", "Grade must be lesser than or equal to 100!", "error");
                    return false;
                }

            },
            afterChange: function (changes, source) {

                if(changes != null){
                    var input_grade_row = changes[0][0];
                    var input_grade_column = changes[0][1];
                    var grading_period_column = changes[0][2];
                    var co_curriculum_id = class_input_id[input_grade_row][input_grade_column];

                    var grade = changes[0][3];
                        if(grade == ""){

                        }
                        $.ajax({
                            url: "../grade_tracking/postUpdateInputCoCurricular",
                            data: {
                                'id': co_curriculum_id, 
                                'grade': grade,
                                'student_id': $('#ig_student_id').val(),
                                'term_id': $('#ig_term_id').val(),
                                '_token': $('input[name=_token]').val(),
                            }, 
                            dataType: 'json',
                            type: 'POST',
                            async:false
                        });
                    }
            },
            className: "htCenter htMiddle",
            height: 450,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            mergeCells: [

                {row: 0, col: 0, rowspan: 1, colspan: 2}
            ],
            cells : function(row, col, prop, changes){
                var cellProperties = {};
                // if(row != 0 || row === 0) {
                //     cellProperties.editor = false;
                // }
                // if (row === 0 || col === 0) {
                //     cellProperties.renderer = firstRowRenderer; // uses function directly
                // }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                // var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                // var input_grade_column = selection[1];
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 || e.keyCode === 190 || e.keyCode === 110 )){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
                
            }
        });
    });
}

function sf10AttendanceBulkHandsonTable() {

    $('#inputgrademodal').on('shown.bs.modal', function (e) {
        $('#sf_10_attendance_bulk_excel').empty();
        // $(".htCore tbody tr").remove();

        var button =  $(e.relatedTarget)
        var ClassificationId = button.data('classification_id')
        var ClassificationLevelId = button.data('level_id')
        var TermId = button.data('term_id')
        var SectionId = button.data('section_id')

        $("#edit_delete").attr('data-section_id',SectionId);
        $("#edit_delete").attr('data-term_id',TermId);
        // modifyAttendance();

        var modal = $(this)
        $('#attendance_bulk_classification_id').val(ClassificationId)
        $('#attendance_bulk_classification_level_id').val(ClassificationLevelId)
        $('#attendance_bulk_term_id').val(TermId)
        $('#attendance_bulk_section_id').val(SectionId)

        $.ajax({
            url:"../grade_tracking/postAttendanceBulk",
            type:'post',
            data:{
                    'term_id': $("#attendance_bulk_term_id").val(),
                    'classification_level_id': $("#attendance_bulk_classification_level_id").val(),
                    'section_id': $("#attendance_bulk_section_id").val(),
                    'student_id': $("#ig_student_id").val(),
                    '_token': $('input[name=_token]').val(),
                },
            async:false
        });

        $.ajax({
            url:"../grade_tracking/dataJsonStudentAttendanceBulk",
            type:'get',
            data:{  
                    'term_id' : TermId,
                    'section_id' : SectionId,
                    'student_id': $("#ig_student_id").val(),
                    'classification_level_id' : ClassificationLevelId,
                },
            dataType: "json",
            async:false,
            success: function (data) { 
                StudentAttendanceArray = data;
            } 
        });

        // $.ajax({
        //     url:"../../attendance_remark/dataJson",
        //     type:'get',
        //     data:{ },
        //     dataType: "json",
        //     async:false,
        //     success: function (data) { 
        //         attendance_remark = data;
        //     }
        // });

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }
        function firstDaysofSchoolRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#FFD08B';
            td.style.color = '#FFFFFF';
        }
        function firstPresentRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#1383A8';
            td.style.color = '#FFFFFF';
        }
        function firstAbsentRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#DD5044';
            td.style.color = '#FFFFFF';
        }
        function firstTardyRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#17A05D';
            td.style.color = '#FFFFFF';
        }
        var data = StudentAttendanceArray[0];
        var student_attendance_id = StudentAttendanceArray[1];

        var container = document.getElementById("sf_10_attendance_bulk_excel");

        if(ClassificationLevelId == 17 || ClassificationLevelId == 18)
        {
            hot = new Handsontable(container, {
                data: data,
                beforeChange: function (changes, source) {

                    //you need to have the perfect score
                    //compare the perfect to the new cell value
                    // alert(parseInt(perfect_score_arr[changes[0][1]]));
                    var days_max = 31;
                    var days_entry = parseInt(changes[0][3]);
                    if(days_entry > days_max ){
                        sweetAlert("Oops...", "Days must be lesser than or equal to 31!", "error");
                        return false;
                    }
                    // if(new_score < new_score_60_percent ){
                    //    $(this).find('td[class="htCenter"]').attr('style','color: red;');
                    
                    // }

                },
                afterChange: function (changes, source) {

                    
                    if(changes != null){
                        var rating_row = changes[0][0];
                        var rating_column = changes[0][1];
                        var student_id = student_attendance_id[rating_row][rating_column];
                        var rating = changes[0][3];
                        $.ajax({
                            url: "../grade_tracking/postStudentAttendanceBulk",
                            data: {
                                'id': student_id,
                                'rating': rating,
                                '_token': $('input[name=_token]').val(),
                            }, 
                            dataType: 'json',
                            type: 'POST',
                            async:false
                        });
                    }
                },
                className: "htCenter htMiddle",
                height: 300,
                currentRowClassName: 'currentRow',
                currentColClassName: 'currentCol',
                fixedRowsTop: 1,
                fillhandle: true,
                fixedColumnsLeft: 3,

                    type: 'numeric',
                    language: 'en',
               
                mergeCells: [
                    {row: 0, col: 3, rowspan: 1, colspan: 4},
                    {row: 0, col: 7, rowspan: 1, colspan: 4},
                    {row: 0, col: 11, rowspan: 1, colspan: 4},
                    {row: 0, col: 15, rowspan: 1, colspan: 4},
                    {row: 0, col: 19, rowspan: 1, colspan: 4},
                    {row: 0, col: 23, rowspan: 1, colspan: 4},
                    {row: 0, col: 27, rowspan: 1, colspan: 4},
                    {row: 0, col: 31, rowspan: 1, colspan: 4},
                    {row: 0, col: 35, rowspan: 1, colspan: 4},
                    {row: 0, col: 39, rowspan: 1, colspan: 4},
                    {row: 0, col: 43, rowspan: 1, colspan: 4}
                ],
                
                cells : function(row, col, prop, td) {
                    var cellProperties = {};

                    if(row === 0 && col <= 46 || col <= 2 ) {
                        cellProperties.editor = false;
                        cellProperties.renderer = firstRowRenderer;
                    }
                    if(row != 0 && col == 3 || row != 0 &&  col == 7 || row != 0 &&  col == 11 || row != 0 &&  col == 15 || row != 0 &&  col == 19 || row != 0 &&  col == 23 || row != 0 &&  col == 27 || row != 0 &&  col == 31 || row != 0 &&  col == 35 || row != 0 &&  col == 39 || row != 0 &&  col == 43){
                         cellProperties.renderer = firstDaysofSchoolRenderer;
                    }
                    if(col == 4 || col == 8 || col == 12 || col == 16 || col == 20 || col == 24 || col == 28 || col == 32 || col == 36 || col == 40 || col == 44){
                         cellProperties.renderer = firstPresentRenderer;
                    }
                    if(col == 5 || col == 9 || col == 13 || col == 17 || col == 21 || col == 25 || col == 29 || col == 33 || col == 37 || col == 41 || col == 45){
                         cellProperties.renderer = firstAbsentRenderer;
                    }
                    if(col == 6 || col == 10 || col == 14 || col == 18 || col == 22 || col == 26 || col == 30 || col == 34 || col == 38 || col == 42 || col == 46){
                         cellProperties.renderer = firstTardyRenderer;
                    }

                    return cellProperties;
                },
                beforeKeyDown: function (e) {
                    var selection = hot.getSelected();
                    //call a function that will check if the e.keyCode corresponds to numeric value
                    // var rating_column = selection[1];
                   
                    
                        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 || e.keyCode === 190 || e.keyCode === 9 || e.keyCode === 110 )){
                            Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                            e.preventDefault();
                        }
                    
                }
            });
        }
        else
        {
            if(TermId >= 16){

                hot = new Handsontable(container, {
                data: data,
                beforeChange: function (changes, source) {

                    //you need to have the perfect score
                    //compare the perfect to the new cell value
                    // alert(parseInt(perfect_score_arr[changes[0][1]]));
                    var days_max = 31;
                    var days_entry = parseInt(changes[0][3]);
                    if(days_entry > days_max ){
                        sweetAlert("Oops...", "Days must be lesser than or equal to 31!", "error");
                        return false;
                    }
                    // if(new_score < new_score_60_percent ){
                    //    $(this).find('td[class="htCenter"]').attr('style','color: red;');
                    
                    // }

                },
                afterChange: function (changes, source) {

                    
                    if(changes != null){
                        var rating_row = changes[0][0];
                        var rating_column = changes[0][1];
                        var student_id = student_attendance_id[rating_row][rating_column];
                        var rating = changes[0][3];
                        $.ajax({
                            url: "../grade_tracking/postStudentAttendanceBulk",
                            data: {
                                'id': student_id,
                                'rating': rating,
                                '_token': $('input[name=_token]').val(),
                            }, 
                            dataType: 'json',
                            type: 'POST',
                            async:false
                        });
                    }
                },
                className: "htCenter htMiddle",
                height: 300,
                currentRowClassName: 'currentRow',
                currentColClassName: 'currentCol',
                fixedRowsTop: 1,
                fillhandle: true,
                fixedColumnsLeft: 3,

                    type: 'numeric',
                    language: 'en',
               
                mergeCells: [
                    {row: 0, col: 3, rowspan: 1, colspan: 4},
                    {row: 0, col: 7, rowspan: 1, colspan: 4},
                    {row: 0, col: 11, rowspan: 1, colspan: 4},
                    {row: 0, col: 15, rowspan: 1, colspan: 4},
                    {row: 0, col: 19, rowspan: 1, colspan: 4},
                    {row: 0, col: 23, rowspan: 1, colspan: 4},
                    {row: 0, col: 27, rowspan: 1, colspan: 4},
                    {row: 0, col: 31, rowspan: 1, colspan: 4},
                    {row: 0, col: 35, rowspan: 1, colspan: 4},
                    {row: 0, col: 39, rowspan: 1, colspan: 4},
                    {row: 0, col: 43, rowspan: 1, colspan: 4}
                ],
                
                cells : function(row, col, prop, td) {
                    var cellProperties = {};

                    if(row === 0 && col <= 46 || col <= 2 ) {
                        cellProperties.editor = false;
                        cellProperties.renderer = firstRowRenderer;
                    }
                    if(row != 0 && col == 3 || row != 0 &&  col == 7 || row != 0 &&  col == 11 || row != 0 &&  col == 15 || row != 0 &&  col == 19 || row != 0 &&  col == 23 || row != 0 &&  col == 27 || row != 0 &&  col == 31 || row != 0 &&  col == 35 || row != 0 &&  col == 39 || row != 0 &&  col == 43){
                         cellProperties.renderer = firstDaysofSchoolRenderer;
                    }
                    if(col == 4 || col == 8 || col == 12 || col == 16 || col == 20 || col == 24 || col == 28 || col == 32 || col == 36 || col == 40 || col == 44){
                         cellProperties.renderer = firstPresentRenderer;
                    }
                    if(col == 5 || col == 9 || col == 13 || col == 17 || col == 21 || col == 25 || col == 29 || col == 33 || col == 37 || col == 41 || col == 45){
                         cellProperties.renderer = firstAbsentRenderer;
                    }
                    if(col == 6 || col == 10 || col == 14 || col == 18 || col == 22 || col == 26 || col == 30 || col == 34 || col == 38 || col == 42 || col == 46){
                         cellProperties.renderer = firstTardyRenderer;
                    }

                    return cellProperties;
                },
                beforeKeyDown: function (e) {
                    var selection = hot.getSelected();
                    //call a function that will check if the e.keyCode corresponds to numeric value
                    // var rating_column = selection[1];
                   
                    
                        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 || e.keyCode === 190 || e.keyCode === 9 || e.keyCode === 110 )){
                            Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                            e.preventDefault();
                        }
                    
                }
            });
            }
            else{
                hot = new Handsontable(container, {
                    data: data,
                    beforeChange: function (changes, source) {

                        //you need to have the perfect score
                        //compare the perfect to the new cell value
                        // alert(parseInt(perfect_score_arr[changes[0][1]]));
                        var days_max = 31;
                        var days_entry = parseInt(changes[0][3]);
                        if(days_entry > days_max ){
                            sweetAlert("Oops...", "Days must be lesser than or equal to 31!", "error");
                            return false;
                        }
                        // if(new_score < new_score_60_percent ){
                        //    $(this).find('td[class="htCenter"]').attr('style','color: red;');
                        
                        // }

                    },
                    afterChange: function (changes, source) {

                        
                        if(changes != null){
                            var rating_row = changes[0][0];
                            var rating_column = changes[0][1];
                            var student_id = student_attendance_id[rating_row][rating_column];
                            var rating = changes[0][3];
                            $.ajax({
                                url: "../grade_tracking/postStudentAttendanceBulk",
                                data: {
                                    'id': student_id,
                                    'rating': rating,
                                    '_token': $('input[name=_token]').val(),
                                }, 
                                dataType: 'json',
                                type: 'POST',
                                async:false
                            });
                        }
                    },
                    className: "htCenter htMiddle",
                    height: 300,
                    currentRowClassName: 'currentRow',
                    currentColClassName: 'currentCol',
                    fixedRowsTop: 1,
                    fillhandle: true,
                    fixedColumnsLeft: 3,

                        type: 'numeric',
                        language: 'en',
                   
                    mergeCells: [
                        {row: 0, col: 3, rowspan: 1, colspan: 4},
                        {row: 0, col: 7, rowspan: 1, colspan: 4},
                        {row: 0, col: 11, rowspan: 1, colspan: 4},
                        {row: 0, col: 15, rowspan: 1, colspan: 4},
                        {row: 0, col: 19, rowspan: 1, colspan: 4},
                        {row: 0, col: 23, rowspan: 1, colspan: 4},
                        {row: 0, col: 27, rowspan: 1, colspan: 4},
                        {row: 0, col: 31, rowspan: 1, colspan: 4},
                        {row: 0, col: 35, rowspan: 1, colspan: 4},
                        {row: 0, col: 39, rowspan: 1, colspan: 4}
                    ],
                    
                    cells : function(row, col, prop, td) {
                        var cellProperties = {};

                        if(row === 0 && col <= 42 || col <= 2 ) {
                            cellProperties.editor = false;
                            cellProperties.renderer = firstRowRenderer;
                        }
                        if(row != 0 && col == 3 || row != 0 &&  col == 7 || row != 0 &&  col == 11 || row != 0 &&  col == 15 || row != 0 &&  col == 19 || row != 0 &&  col == 23 || row != 0 &&  col == 27 || row != 0 &&  col == 31 || row != 0 &&  col == 35 || row != 0 &&  col == 39){
                             cellProperties.renderer = firstDaysofSchoolRenderer;
                        }
                        if(col == 4 || col == 8 || col == 12 || col == 16 || col == 20 || col == 24 || col == 28 || col == 32 || col == 36 || col == 40){
                             cellProperties.renderer = firstPresentRenderer;
                        }
                        if(col == 5 || col == 9 || col == 13 || col == 17 || col == 21 || col == 25 || col == 29 || col == 33 || col == 37 || col == 41){
                             cellProperties.renderer = firstAbsentRenderer;
                        }
                        if(col == 6 || col == 10 || col == 14 || col == 18 || col == 22 || col == 26 || col == 30 || col == 34 || col == 38 || col == 42){
                             cellProperties.renderer = firstTardyRenderer;
                        }

                        return cellProperties;
                    },
                    beforeKeyDown: function (e) {
                        var selection = hot.getSelected();
                        //call a function that will check if the e.keyCode corresponds to numeric value
                        // var rating_column = selection[1];
                       
                        
                            if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 || e.keyCode === 190 || e.keyCode === 9 || e.keyCode === 110 )){
                                Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                                e.preventDefault();
                            }
                        
                    }
                });

            }
           
        }
    });
}