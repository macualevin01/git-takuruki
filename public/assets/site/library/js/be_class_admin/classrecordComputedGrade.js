var hot;

$(document).ready(function () {
    gradeComputationHandsonTable();
});

function excelGradeComputation(reportId) {
    var class_id = $('#gc_class_id').val();
    var term_id = $('#gc_term_id').val();
    var semester_level_id = $('#gc_semester_level_id').val();
    var classification_level_id = $('#gc_classification_level_id').val();
    var url = $("#"+reportId).data('url');
    // alert(class_id);
    url = url +"?class_id="+class_id+"&term_id="+term_id+"&semester_level_id="+semester_level_id+"&classification_level_id="+classification_level_id;

    window.open(url);
}

function gradeComputationHandsonTable() {

    $('#ClassRecordGradeComputation').on('shown.bs.modal', function (e) {

        $(".htCore tbody tr").remove();

        var button =  $(e.relatedTarget)
        var Id = button.data('id')
        var Level = button.data('level')
        var SectionName = button.data('section_name')
        var Name = button.data('name')
        var ClassificationId = button.data('classification_id')
        var ClassificationLevelId = button.data('classification_level_id')
        var TermId = button.data('term_id')
        var SemesterLevelId = button.data('semester_level_id')

        var modal = $(this)
        $('.grade_computation_pace').text('Computation of Grade - ' + Level +' '+SectionName+' ('+Name+')')
        $('.grade_computation').text('Computation of Grade - ' + Level +' '+SectionName+' ('+Name+')')
        $('#gc_class_id').val(Id)
        $('#gc_classification_id').val(ClassificationId)
        $('#gc_classification_level_id').val(ClassificationLevelId)
        $('#gc_term_id').val(TermId)
        $('#gc_semester_level_id').val(SemesterLevelId)

        $.ajax({
            url:"teachers_portal/dataJsonClassRecordGradeComputation",
            type:'get',
            data:
                {  
                    'class_id' : Id,
                    'classification_id' : ClassificationId,
                    'term_id' : TermId,
                    'classification_level_id' : ClassificationLevelId,
                    'semester_level_id' : SemesterLevelId
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                paceArray = data;

                // console.log(paceArray);
            } 

        });
        $.ajax({
            url:"admin/gradeComputationPassorFailed",
            type:'get',
            data:
                {  
                    'class_id' : Id,
                    'classification_id' : ClassificationId,
                    'term_id' : TermId,
                    'semester_level_id' : SemesterLevelId
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                passOrFailed = data;
            }

        });
        $.ajax({
            url:"admin/clubDefine",
            type:'get',
            data:
                {  
                    'class_id' : Id,
                    'classification_id' : ClassificationId,
                    'term_id' : TermId,
                    'semester_level_id' : SemesterLevelId
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                ClubDefine = data;
            }

        });

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }
        function failRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.color = 'red';
        }

        var data = paceArray[0];
        var color_arr = passOrFailed[0];
        var club_define = ClubDefine;
        // var grade_id_arr = paceArray[1];

        if(SemesterLevelId == 2 || SemesterLevelId == 3 || club_define == 1){
        var container = document.getElementById("grade_computation_excel");
        hot = new Handsontable(container, {
            data: data,
            className: "htCenter htMiddle",
            height: 800,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            mergeCells: [

                {row: 0, col: 7, rowspan: 1, colspan: 3},
                {row: 0, col: 8, rowspan: 1, colspan: 3},
                {row: 0, col: 9, rowspan: 1, colspan: 3}
            ],
            cells : function(row, col, prop, td){
                var cellProperties = {};
                
                if(row != 0 && row >= 1){
                    if(col != 0 && col >= 5 && col <= 9){

                        if(color_arr[row][col] == 'failed'){
                            cellProperties.renderer = failRenderer;

                        }
                        // alert(row+" "+col)
                    }
                    else{

                    }
                    
                }
                else{
                    // cellProperties.renderer = successRenderer;
                }
                
                if(row != 0 || row === 0) {
                    cellProperties.editor = false;
                }
                if (row === 0 || col === 0) {
                    cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13)){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
            }
        });
        }
        else{
        var container = document.getElementById("grade_computation_excel");
        hot = new Handsontable(container, {
            data: data,
            className: "htCenter htMiddle",
            height: 800,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            mergeCells: [

                {row: 0, col: 8, rowspan: 1, colspan: 3},
                {row: 0, col: 9, rowspan: 1, colspan: 3},
                {row: 0, col: 10, rowspan: 1, colspan: 3}
            ],
            cells : function(row, col, prop, td){
                var cellProperties = {};

                if(row != 0 && row >= 1){
                    if(col != 0 && col >= 5 && col <= 10){

                        if(color_arr[row][col] == 'failed'){
                            cellProperties.renderer = failRenderer;

                        }
                        // alert(row+" "+col)
                    }

                    else{

                    }
                    
                }
                else{
                    // cellProperties.renderer = successRenderer;
                }
                
                if(row != 0 || row === 0) {
                    cellProperties.editor = false;
                }
                if (row === 0 || col === 0) {
                    cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13)){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
            }
        }); 
        }

    });
}
