var hot;

$(document).ready(function () {
    ComputedGrades();
});


function ComputedGrades() {

    $('#computedGrades').on('shown.bs.modal', function (e) {

        var button =  $(e.relatedTarget)
        var Id = button.data('id')
        var Level = button.data('level')
        var SectionID = button.data('section_id')
        var Name = button.data('name')
        var ClassificationId = button.data('classification_id')
        var ClassificationLevelId = button.data('classification_level_id')
        var TermId = button.data('term_id')
        var GradingPeriodId = button.data('grading_period_id')
        var SemesterLevelId = button.data('semester_level_id')

        getComputedGrades(Id,Level,SectionID,Name,ClassificationId,ClassificationLevelId,TermId,SemesterLevelId,GradingPeriodId);


    
    });
}

function getComputedGrades(Id,Level,SectionID,Name,ClassificationId,ClassificationLevelId,TermId,SemesterLevelId,GradingPeriodId)
    {
 
      $.ajax({
        url: 'admin/coputedGreades',
        type: 'get',
        data: {
          class_id:Id,
          level:Level,
          section_id:SectionID,
          student_id:Name,
          classification_id:ClassificationId,
          classification_level_id:ClassificationLevelId,
          term_id:TermId,
          semester_level_id:SemesterLevelId,
          grading_period_id:GradingPeriodId,
        }
      }).done(function(computed_grade_data){

        $('#computed_grades_container').html(computed_grade_data);
      })
    }
