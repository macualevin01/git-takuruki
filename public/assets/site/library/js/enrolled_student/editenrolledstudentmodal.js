$(document).ready(function(){
    $('#editEnrolledStudent').on('shown.bs.modal', function(e) {   

        var button =  $(e.relatedTarget)
        var EnrollmentId = button.data('enrollment_id')
        var StudentId = button.data('student_id')
        var LastName = button.data('last_name')
        var FirstName = button.data('first_name')
        var MiddleName = button.data('middle_name')
        var ClubId = button.data('club_id')

        var modal = $(this)
        $('.edit_enrolled_student_modal').text('Edit Student Club')
        $('#student_id').val(StudentId)
        $('#enrollment_id').val(EnrollmentId)
        $('#edit_enrolled_student_name').text(LastName+', '+FirstName+' '+MiddleName)
        $('#edit_enrolled_student_club').val(ClubId)

        if($('#gate_pass').val() == 1){
            $('#is_gate_pass').attr('checked','true');
        }
        else{
            $('#is_gate_pass').removeAttr('checked','true');
        }
        if($('#lunch_pass').val() == 1){
            $('#is_lunch_pass').attr('checked','true');
        }
        else{
            $('#is_lunch_pass').removeAttr('checked','true');
        }
        // $.ajax({
        //       url:"enroll_student/editdataJson",
        //       type:'get',
        //       data:
        //           {  
        //             'student_id' : StudentId
        //           },
        //       dataType: "json",
        //       async:false

        // }).done(function(data) {
            
        //     if(data.length > 0)
        //     {
        //         $.each( data, function( key, item ) {

        //             $("#edit_enrolled_student_payment_scheme").val(item.payment_scheme_id);
        //         });
        //     }
        // });

     // var payment_scheme_id = $("#edit_enrolled_student_payment_scheme").val();
     //    $.ajax({
     //        url:"payment_scheme/dataJson",
     //        type:'GET',
     //        data:
     //            {  
     //                'id': $("#payment_scheme_id").val(),
     //            },
     //        dataType: "json",
     //        async:false,
     //        success: function (data) 
     //        {    
                
     //            $("#edit_enrolled_student_payment_scheme").empty();
     //            $("#edit_enrolled_student_payment_scheme").append('<option value=""></option>');
     //            $.map(data, function (item) 
     //            {       
     //                    payment_scheme_name = item.text;

     //                    $("#edit_enrolled_student_payment_scheme").append('<option value="'+item.value+'">'+payment_scheme_name+'</option>');
     //            });
     //        }  
     //    });

        
    });
        $(".EditEnrolledStudent").click(function () {
            $.ajax({
                url:"enroll_student/postEditdataJson",
                type:'post',
                data:{ 
                        'id': $("#enrollment_id").val(),
                        'club_id': $("#edit_enrolled_student_club").val(),
                        'semester_level_id': $("#edit_enrolled_semester_level_club").val(),
                        'term_id': $("#edit_enrolled_term_club").val(),
                        '_token': $('input[name=_token]').val(),
                    },
                async:false
            });
            $("#editEnrolledStudent").modal('hide');
            swal("Edited!", "Successfully Edited Enrolled Student", "success"); 
            $('#table').dataTable().fnReloadAjax("enroll_student/data?level="+$("#classification_level_id").val()+"&term_id="+$("#term_id").val()+"&semester_level_id="+$("#semester_level_id").val());

        });

        $(".DeleteEnrolledStudent").click(function () {
            $.ajax({
                url:"enroll_student/postDeletedataJson",
                type:'post',
                data:{ 
                        'id': $("#enrollment_id").val(),
                        'club_id': $("#edit_enrolled_student_club").val(),
                        'semester_level_id': $("#edit_enrolled_semester_level_club").val(),
                        'term_id': $("#edit_enrolled_term_club").val(),
                        '_token': $('input[name=_token]').val(),
                    },
                async:false
            });
            $("#editEnrolledStudent").modal('hide');
            swal("Deleted", "Successfully Delete", "error");  
            $('#table').dataTable().fnReloadAjax("enroll_student/data?level="+$("#classification_level_id").val()+"&term_id="+$("#term_id").val()+"&semester_level_id="+$("#semester_level_id").val());

        });

    });
// });