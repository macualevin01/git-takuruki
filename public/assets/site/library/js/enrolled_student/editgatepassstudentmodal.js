$(document).ready(function(){
    $('#editGatePassStudent').on('shown.bs.modal', function(e) {   

        var button =  $(e.relatedTarget)
        var EnrollmentId = button.data('enrollment_id')
        var StudentId = button.data('student_id')
        var LastName = button.data('last_name')
        var FirstName = button.data('first_name')
        var MiddleName = button.data('middle_name')
        var is_gate_pass = button.data('is_gate_pass')
        var is_lunch_pass = button.data('is_lunch_pass')
        
        var modal = $(this)
        $('#gate_pass_student_id').val(StudentId)
        $('#gate_pass_enrollment_id').val(EnrollmentId)
        $('#lunch_pass').val(is_lunch_pass)
        $('#gate_pass').val(is_gate_pass)
        $('.edit_gate_pass_student_modal').text('Edit Student Pass '+LastName+', '+FirstName+' '+MiddleName)

        if($('#gate_pass').val() == 1){
            $('#is_gate_pass').attr('checked','true');
        }
        else{
            $('#is_gate_pass').removeAttr('checked','true');
        }
        if($('#lunch_pass').val() == 1){
            $('#is_lunch_pass').attr('checked','true');
        }
        else{
            $('#is_lunch_pass').removeAttr('checked','true');
        }
        
    });

    $('#is_lunch_pass').change(function(){
        if($('#is_lunch_pass').is(':checked')){
            $.ajax({
                url:"enroll_student/postEditLUNCHPASS",
                type:'post',
                data:{ 
                        'id': $("#gate_pass_enrollment_id").val(),
                        'pass': 1,
                        '_token': $('input[name=_token]').val(),
                    },
                async:false
            });
        }
        else{
            $.ajax({
                url:"enroll_student/postEditLUNCHPASS",
                type:'post',
                data:{ 
                        'id': $("#gate_pass_enrollment_id").val(),
                        'pass': 0,
                        '_token': $('input[name=_token]').val(),
                    },
                async:false
            });
        }

    });
        $('#is_gate_pass').change(function(){
            if($('#is_gate_pass').is(':checked')){
                $.ajax({
                    url:"enroll_student/postEditGATEPASS",
                    type:'post',
                    data:{ 
                            'id': $("#gate_pass_enrollment_id").val(),
                            'pass': 1,
                            '_token': $('input[name=_token]').val(),
                        },
                    async:false
                });
            }
            else{
                $.ajax({
                    url:"enroll_student/postEditGATEPASS",
                    type:'post',
                    data:{ 
                            'id': $("#gate_pass_enrollment_id").val(),
                            'pass': 0,
                            '_token': $('input[name=_token]').val(),
                        },
                    async:false
                });

            }
        });

});
        $(".close").click(function(){
            $('#editGatePassStudent').modal('hide');
            $('#table').dataTable().fnReloadAjax("enroll_student/data?level="+$("#classification_level_id").val()+"&term_id="+$("#term_id").val()+"&semester_level_id="+$("#semester_level_id").val());
        });


        $('#editGatePassStudent').on('hidden.bs.modal', function () {
            $('#table').dataTable().fnReloadAjax("enroll_student/data?level="+$("#classification_level_id").val()+"&term_id="+$("#term_id").val()+"&semester_level_id="+$("#semester_level_id").val());
        });

