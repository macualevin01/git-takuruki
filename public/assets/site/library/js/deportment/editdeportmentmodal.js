var hot;

$(document).ready(function () {
    EditDeportmentHandsonTable();

});

function EditDeportmentHandsonTable() {

    $('#editdeportmentmodal').on('shown.bs.modal', function (e) {
            $("#edit_deportment_excel").empty();
            var button =  $(e.relatedTarget)
             // var ClassificationId = button.data('classification_id')
            var StudentId = button.data('student_id')
            var ClassificationLevelId = button.data('classification_level_id')
            var TermId = button.data('term_id')
            var SectionId = button.data('section_id')
            var Level = button.data('level')
            var SectionName = button.data('section_name')
            var TermName = button.data('term_name')
            var FirstName = button.data('first_name')
            var LastName = button.data('last_name')
            var MiddleName = button.data('middle_name')

            var modal = $(this)
            $('.edit_deportment_record').text('Component Grade of ' +LastName+", "+FirstName+" "+MiddleName)
            $(this).find('#edit_student_deportment_student_id').val(StudentId);
            $(this).find('#edit_student_deportment_classification_level_id').val(ClassificationLevelId);
            $(this).find('#edit_student_deportment_term_id').val(TermId);
            $(this).find('#edit_student_deportment_section_id').val(SectionId);

            $.ajax({
                url:"../class_record/ScoreEntryLockOrUnlock",
                type:'get',
                data:{ 
                    'grading_period_id' : $('#edit_student_deportment_grading_period_id').val(),
                    'class_id' : $('#class_index_id').val()
                },
                dataType: "json",
                async:false,
                success: function (data){
                    Security = data;
                }
            });
            //code for insert data automatically
            // $.ajax({
            //     url:"../teachers_portal/deportment/create",
            //     type:'post',
            //     data: 
            //         { 
                        
            //             'term_id': $("#edit_student_deportment_term_id").val(),
            //             'section_id': $("#edit_student_deportment_section_id").val(),
            //             'subject_id': $("#edit_student_deportment_subject_id").val(),
            //             'grading_period_id': $("#edit_student_deportment_grading_period_id").val(),
            //             'classification_level_id': $("#edit_student_deportment_classification_level_id").val(),
            //             'student_id': $("#edit_student_deportment_student_id").val(),
            //             '_token': $('input[name=_token]').val(),
                                                      
            //         },
            //     async:false
            // });

            $.ajax({
                url:"../teachers_portal/deportment/dataJsonEdit",
                type:'get',
                data:{  
                        'grading_period_id' : $("#edit_student_deportment_grading_period_id").val(),
                        'classification_level_id' : $("#edit_student_deportment_classification_level_id").val(),
                        'grade_level_adviser' : $("#grade_level_adviser").val(),
                        'student_id' : $("#edit_student_deportment_student_id").val(),
                        'subject_id' : $("#edit_student_deportment_subject_id").val(),
                        'section_id' : $("#edit_student_deportment_section_id").val(),
                        'term_id' : $("#edit_student_deportment_term_id").val(),
                        'class_id' : $("#class_index_id").val(),
                        'teacher_section_id' : $("#teacher_section_id").val(),
                        '_token': $('input[name=_token]').val(),
                    },
                dataType: "json",
                async:false,
                success: function (data){ 
                    ScoreEntryJson = data;
                } 
            });
            var data = ScoreEntryJson[0];
            var score_entry_id_arr = ScoreEntryJson[1];
            var security_data = Security;

            if(security_data == 1){

                sweetAlert("Oops...", "You are not allowed to do something!", "error");
                // return false;
            }
            
            function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
            }

            var container = document.getElementById("edit_deportment_excel");
            hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {

                //you need to have the perfect score
                //compare the perfect to the new cell value
                // alert(parseInt(perfect_score_arr[changes[0][1]]));
                var perfect_score = 100;
                var new_score = parseInt(changes[0][3]);
                var sixty_percent = parseInt(.60);
                var new_score_60_percent = perfect_score * .6 ;
                if(new_score > perfect_score ){
                    sweetAlert("Oops...", "Score must be lesser than or equal to perfect score!", "error");
                    return false;
                }
                // if(new_score < new_score_60_percent ){
                //    $(this).find('td[class="htCenter"]').attr('style','color: red;');
                
                // }

            },
            afterChange: function (changes, source) {

                if(changes != null){
                    var score_entry_row = changes[0][0];
                    var score_entry_column = changes[0][1];
                    var score_entry_id = score_entry_id_arr[score_entry_row][score_entry_column];
                    var new_score = changes[0][3];
                    
                        if(new_score == ""){
                            $.ajax({
                                url: "../teachers_portal/deportment/postUpdateScoreEntry",
                                data: {
                                    'id': score_entry_id, 
                                    'score': new_score,
                                    '_token': $('input[name=_token]').val(),
                                },
                                dataType: 'json',
                                type: 'POST',
                                async:false
                            });
                        }
                        else{
                           $.ajax({
                                url: "../teachers_portal/deportment/postUpdateScoreEntry",
                                data: {
                                    'id': score_entry_id, 
                                    'score': new_score,
                                    '_token': $('input[name=_token]').val(),
                                },
                                dataType: 'json',
                                type: 'POST',
                                async:false
                            }); 
                           
                           
                        }

                    
                }
            },
            className: "htCenter htMiddle",
            height: 450,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 2,
            mergeCells: [
                {row: 0, col: 0, rowspan: 1, colspan: 1},
                {row: 2, col: 2, rowspan: 1, colspan: 1}
            ],
            cells: function (row, col, prop, changes) {
                var cellProperties = {};
                if(security_data == 1){
                    if (row > 0 && col > 0) {
                        cellProperties.editor = false;
                        cellProperties.renderer = firstRowRenderer; 
                    }
                }
                else{
                    if(row === 0 || row === 1 || col === 0 || col === 1 ) {
                        cellProperties.editor = false;
                    }
                    if (row === 0 || row === 1 || col === 0) {
                        cellProperties.renderer = firstRowRenderer; // uses function directly
                    }
                }
                

              return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                var score_entry_column = selection[1];
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 )){
                        Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                        e.preventDefault();
                    }
                
            }
        });
    });     
};