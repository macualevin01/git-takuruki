var oTable;
$(document).ready(function () {

        $('#gradingperioddeportmentselfevaluationmodal').on('shown.bs.modal', function(e) { 
            
            $('#gradingperiod_se_table').addClass('hidden');
            $('#se_term_id').val('');
            $('#se_semester_level_id').val('');
            var button =  $(e.relatedTarget)
            var teacher_section_name = button.data('section_name')
            var teacher_classification_level_id = button.data('classification_level_id')
            var teacher_section_id = button.data('section_id')
            $('#se_section_id').val(teacher_section_id);
            $('#teacher_section_deportment_id').val(teacher_section_id);
            $('#teacher_classification_level_deportment_id').val(teacher_classification_level_id);
            $('#se_classification_level_id').val(teacher_classification_level_id);
            $('.grading_period_se').text('Homeroom Adviser of '+teacher_section_name);
        });

    oTable = $('#gradingperiod_se_table').dataTable({
        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "bProcessing": true,
        "bServerSide": true,
        "bStateSave": true,
        "sAjaxSource": "../teachers_portal/deportment/gradingPeriodDataSE",
        "fnDrawCallback": function ( oSettings ) {
        },
        "fnServerParams": function(aoData){
            aoData.push(
                { "name":"term_id", "value": $("#se_term_id").val()},
                { "name":"semester_level_id", "value": $("#se_semester_level_id").val()},
                { "name":"section_id", "value": $("#se_section_id").val()},
                { "name":"classification_level_id", "value": $("#se_classification_level_id").val()}
            );
        }
    });
    $("#se_term_id").change(function(){
             oTable.fnDraw();
          });
    $("#se_semester_level_id").change(function(){
             oTable.fnDraw();
          });
});
        $("#se_semester_level_id, #se_term_id").change(function(){
            if($('#se_semester_level_id').val() == 0 && $('#se_term_id').val() == 0 && $('#se_semester_level_id').val() == "" && $('#se_term_id').val() == ""){
                $('#gradingperiod_se_table').addClass('hidden');
            }
            else{
                $('#gradingperiod_se_table').removeClass('hidden');
            }
        });

// function EditDeportmentHandsonTable() {

//     $('#editdeportmentmodal').on('shown.bs.modal', function (e) {

//         var button =  $(e.relatedTarget)
//              // var ClassificationId = button.data('classification_id')
//             var StudentId = button.data('student_id')
//             var ClassificationLevelId = button.data('classification_level_id')
//             var TermId = button.data('term_id')
//             var SectionId = button.data('section_id')
//             var Level = button.data('level')
//             var SectionName = button.data('section_name')
//             var TermName = button.data('term_name')
//             var FirstName = button.data('first_name')
//             var LastName = button.data('last_name')
//             var MiddleName = button.data('middle_name')

//             var modal = $(this)
//             $('.edit_deportment_record').text('Component Grade of ' +LastName+", "+FirstName+" "+MiddleName+" - "+ Level+" "+SectionName+" S.Y "+TermName)
//             $(this).find('#edit_student_deportment_student_id').val(StudentId);
//             $(this).find('#edit_student_deportment_classification_level_id').val(ClassificationLevelId);
//             $(this).find('#edit_student_deportment_term_id').val(TermId);
//             $(this).find('#edit_student_deportment_section_id').val(SectionId);
       
// }