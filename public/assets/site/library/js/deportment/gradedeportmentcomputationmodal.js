var hot;

$(document).ready(function () {
    gradeDeportmentComputationHandsonTable();
});

function gradeDeportmentComputationHandsonTable() {

    $('#gradedeportmentcomputationmodal').on('shown.bs.modal', function (e) {

        $(".htCore tbody tr").remove();
        $("#grade_deportment_computation_excel").empty();
        var button =  $(e.relatedTarget)
        var Id = button.data('id')
        var Level = button.data('level')
        var SectionName = button.data('section_name')
        var Name = button.data('name')
        var ClassificationId = button.data('classification_id')
        var ClassificationLevelId = button.data('classification_level_id')
        var TermId = button.data('term_id')
        var SectionId = button.data('section_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SubjectId = button.data('subject_id')

        var modal = $(this)
        $('.grade_computation_pace').text('Computation of Grade - ' + Level +' '+SectionName+' ('+Name+')')
        $('#gc_class_id').val(Id)
        $('#gc_classification_id').val(ClassificationId)
        $('#gc_term_id').val(TermId)

        $.ajax({
            url:"../teachers_portal/deportment/dataJsonDeportmentGradeComputation",
            type:'get',
            data:
                {  
                    'class_id' : Id,
                    'section_id' : SectionId,
                    'semester_level_id' : SemesterLevelId,
                    'subject_id' : SubjectId,
                    'classification_id' : ClassificationId,
                    'classification_level_id' : ClassificationLevelId,
                    'term_id' : TermId
                },
            dataType: "json",
            async:false,
            success: function (data) 
            { 
                paceArray = data;
            } 

        });

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }

        var data = paceArray;
        if(SemesterLevelId == 2 || SemesterLevelId == 3){
        var container = document.getElementById("grade_deportment_computation_excel");
        hot = new Handsontable(container, {
            data: data,
            className: "htCenter htMiddle",
            height: 300,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            mergeCells: [
                {row: 0, col: 7, rowspan: 1, colspan: 3},
                {row: 0, col: 8, rowspan: 1, colspan: 3},
                {row: 0, col: 9, rowspan: 1, colspan: 3}
            ],
            cells : function(row, col, prop, td){
                var cellProperties = {};
                if(row != 0 || row === 0) {
                    cellProperties.editor = false;
                }
                if (row === 0 || col === 0) {
                    cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13)){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
            }
        });   
        }
        else{
            var container = document.getElementById("grade_deportment_computation_excel");
        hot = new Handsontable(container, {
            data: data,
            className: "htCenter htMiddle",
            height: 300,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            mergeCells: [
                {row: 0, col: 8, rowspan: 1, colspan: 3},
                {row: 0, col: 9, rowspan: 1, colspan: 3},
                {row: 0, col: 10, rowspan: 1, colspan: 3}
            ],
            cells : function(row, col, prop, td){
                var cellProperties = {};
                if(row != 0 || row === 0) {
                    cellProperties.editor = false;
                }
                if (row === 0 || col === 0) {
                    cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13)){
                    Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    e.preventDefault();
                }
            }
        });
        }
    });
}