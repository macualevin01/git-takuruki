$(document).ready(function () {

    $('#studentdeportmentmodal').on('shown.bs.modal', function(e) { 

        var button =  $(e.relatedTarget)
        // var ClassificationId = button.data('classification_id')
        var ClassificationLevelId = button.data('classification_level_id')
        var TermId = button.data('term_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SectionId = button.data('section_id')
        var SubjectId = button.data('subject_id')
        var Level = button.data('level')
        var SectionName = button.data('section_name')
        var TermName = button.data('term_name')
        var GradingPeriodId = button.data('id')

        var modal = $(this)
        // $('.student_list_modal').text('Student List : ' + Level+' '+SectionName)
        // $('#student_list_classification_id').val(ClassificationId)
        $('#student_list_classification_level_id').val(ClassificationLevelId)
        $('#student_list_section_id').val(SectionId)
        $('#student_list_term_id').val(TermId)
        $('#student_list_semester_level_id').val(SemesterLevelId)
        $('#edit_student_deportment_subject_id').val(SubjectId)
        $('#grading_period_deportment_subject_id').val(SubjectId)

        $('#edit_student_deportment_grading_period_id').val(GradingPeriodId)

        $.ajax({
                url:"../teachers_portal/deportment/create",
                type:'post',
                data: 
                    { 
                        
                        'class_id': $("#class_index_id").val(),
                        'term_id': $("#student_list_term_id").val(),
                        'semester_level_id': $("#student_list_semester_level_id").val(),
                        'section_id': $("#student_list_section_id").val(),
                        'subject_id': $("#edit_student_deportment_subject_id").val(),
                        'grading_period_id': GradingPeriodId,
                        'classification_level_id': $("#student_list_classification_level_id").val(),
                        '_token': $('input[name=_token]').val(),
                                                      
                    },
                async:false
            });

        studentlisttable = $('#studentlisttable').dataTable({
            "sDom" : "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType" : "bootstrap",
            "bProcessing" : true,
            "bServerSide" : true,
            "bStateSave": true,
            "sAjaxSource" : "../teachers_portal/deportment/student_list_data",
            "fnDrawCallback": function ( oSettings ) {
            },
            "fnServerParams": function(aoData){
                aoData.push(
                    // { "name":"classification_id", "value": $("#student_list_classification_id").val() },
                    { "name":"classification_level_id", "value": $("#student_list_classification_level_id").val() },
                    { "name":"term_id", "value": $("#student_list_term_id").val() },
                    { "name":"semester_level_id", "value": $("#student_list_semester_level_id").val() },
                    { "name":"class_id", "value": $("#class_index_id").val() },
                    { "name":"section_id", "value": $("#student_list_section_id").val() }
                );
            }
        });
    });



    
});