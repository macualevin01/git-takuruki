var hot;

$(document).ready(function () {
    $('#gradingperioddeportmentmodal').on('shown.bs.modal', function(e) {   
  
        var button =  $(e.relatedTarget)
        var ClassificationLevelId = button.data('classification_level_id')
        var TermId = button.data('term_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SectionId = button.data('section_id')
        var SubjectId = button.data('subject_id')
        var Level = button.data('level')
        var SectionName = button.data('section_name')
        var SubjectName = button.data('name')
        var TermName = button.data('term_name')
        var ClassId = button.data('id')

        

        var modal = $(this)
        $('.all_deportment_record').text('All Student Deportment in '+ Level +' '+SectionName)
        $('.grading_period').text('Grading Period - ' + Level +' '+SectionName)
        $('.grading_period_subject').text(SubjectName)
        $('.student_list_modal').text('Student List : ' + Level+' '+SectionName)
        // $('#student_list_classification_id').val(ClassificationId)
        $('#class_index_id').val(ClassId)
        $('#student_list_classification_level_id').val(ClassificationLevelId)
        $('#student_list_section_id').val(SectionId)
        $('#student_list_term_id').val(TermId)
        $('#edit_student_deportment_subject_id').val(SubjectId)
        $('#student_list_subject_id').val(SubjectId)
        $('#grading_period_deportment_subject_id').val(SubjectId)
        $('#grading_period_deportment_semester_level_id').val(SemesterLevelId)
        $('#grading_period_deportment_section_id').val(SectionId)


        $('#all_student_deportment_classification_level_id').val(ClassificationLevelId)
        $('#all_student_deportment_term_id').val(TermId)
        $('#all_student_deportment_semester_level_id').val(SemesterLevelId)
        $('#all_student_deportment_section_id').val(SectionId)
        $('#all_student_deportment_subject_id').val(SubjectId)


        gradingperiodtable.fnDraw();
    });



    gradingperiodtable = $('#gradingperiodtable').dataTable( {
        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "bProcessing": true,
        "bServerSide": true,
        "bStateSave": true,
        "sAjaxSource": "../teachers_portal/deportment/grading_period_data",
        "fnDrawCallback": function ( oSettings ) {
        },
        "fnServerParams": function(aoData){
            aoData.push(
                { "name":"class_id", "value": $("#grading_period_deportment_class_id").val() },
                { "name":"term_id", "value": $("#grading_period_deportment_term_id").val() },
                { "name":"semester_level_id", "value": $("#grading_period_deportment_semester_level_id").val() },
                { "name":"classification_id", "value": $("#grading_period_deportment_classification_id").val() },
                { "name":"section_id", "value": $("#grading_period_deportment_section_id").val() },
                { "name":"subject_id", "value": $("#grading_period_deportment_subject_id").val() }
            );
        }
    });
});

// function EditDeportmentHandsonTable() {

//     $('#editdeportmentmodal').on('shown.bs.modal', function (e) {

//         var button =  $(e.relatedTarget)
//              // var ClassificationId = button.data('classification_id')
//             var StudentId = button.data('student_id')
//             var ClassificationLevelId = button.data('classification_level_id')
//             var TermId = button.data('term_id')
//             var SectionId = button.data('section_id')
//             var Level = button.data('level')
//             var SectionName = button.data('section_name')
//             var TermName = button.data('term_name')
//             var FirstName = button.data('first_name')
//             var LastName = button.data('last_name')
//             var MiddleName = button.data('middle_name')

//             var modal = $(this)
//             $('.edit_deportment_record').text('Component Grade of ' +LastName+", "+FirstName+" "+MiddleName+" - "+ Level+" "+SectionName+" S.Y "+TermName)
//             $(this).find('#edit_student_deportment_student_id').val(StudentId);
//             $(this).find('#edit_student_deportment_classification_level_id').val(ClassificationLevelId);
//             $(this).find('#edit_student_deportment_term_id').val(TermId);
//             $(this).find('#edit_student_deportment_section_id').val(SectionId);
       
// }