$(document).ready(function(){
        $('#interventionmodal').on('shown.bs.modal', function(e) {   

            var button =  $(e.relatedTarget)
            var Id = button.data('id')
            var Level = button.data('level')
            var SectionName = button.data('section_name')
            var Name = button.data('name')

            var modal = $(this)
            $('.intervention').text('Intervention - ' + Level +' '+SectionName+' ('+Name+')')
            $('#intervention_class_id').val(Id)

            interventiontable.fnDraw();
            $.ajax({
                url:"../../grading_period/dataJsonGradingPeriodIntervention",
                type:'GET',
                data:
                    {  
                        'class_id': $("#intervention_class_id").val(),
                    },
                dataType: "json",
                async:false,
                success: function (data) 
                {    
                    
                    $("#intervention_grading_period_id").empty();
                    $("#intervention_grading_period_id").append('<option value=""></option>');
                    $.map(data, function (item) 
                    {      
                        if(item.value <= 3)
                        {
                            $("#intervention_grading_period_id").append('<option value="'+item.value+'">'+item.text+'</option>');
                        }
                        else{
                            $("#intervention_grading_period_id").append('<option value="'+item.value+'">'+item.text+' '+item.description+'</option>');
                        }
                    });
                }  

            });
        });

            interventiontable = $('#interventiontable').dataTable( {
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "bProcessing": true,
                "bServerSide": true,
                "bStateSave": true,
                "sAjaxSource": "../../teachers_portal/intervention_data",

                "fnDrawCallback": function ( oSettings ) {
                     $.ajax({
                        url:"../../teachers_portal/grade_remark",
                        data : {
                            'class_id':  $("#intervention_class_id").val(),
                            'grading_period_id' : $("#intervention_grading_period_id").val()
                        },
                        type:'get',
                        dataType: "json",
                        async:false,
                        success: function (data){ 
                            $.map(data,function(item){ 
                                $('#'+item.student_id+'_grade').text(item.letter_grade+' '+item.transmuted_grade);
                                $('#'+item.student_id+'_absent').text(item.total_absent);
                                $('#'+item.student_id+'_late').text(item.total_tardy);
                            });
                        } 
                    });                
                },
                "fnServerParams": function(aoData){
                    aoData.push(
                        { "name":"class_id", "value": $("#intervention_class_id").val() },
                        { "name":"grading_period_id", "value": $("#intervention_grading_period_id").val() }
                    );
                }
            });
            $("#intervention_grading_period_id").change(function() {
                myFunction();
                });
            function myFunction() {
            // demoP.innerHTML = demoP.innerHTML + "index[" + index + "]: " + item + "<br />"; 
            // $('#table_classification_'+item).dataTable().fnDraw();
            $('#interventiontable').dataTable().fnReloadAjax("../../teachers_portal/intervention_data?class_id="+$("#intervention_class_id").val()+"&grading_period_id="+$("#intervention_grading_period_id").val());
            // $('#table_classification_'+item).dataTable()._fnAjaxUpdate();
            }
});

        