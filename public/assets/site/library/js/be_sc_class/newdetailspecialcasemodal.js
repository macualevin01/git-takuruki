$(document).ready(function(){
		$('#newdetailspecialcasemodal').on('show.bs.modal', function(e) { 

            var button =  $(e.relatedTarget)
            var ClassStandingComponentId = button.data('id')
            var ClassComponentCategoryName = button.data('ccc_name')
            var classId = button.data('class_id')

            var modal = $(this)
            $('.new_detail').text('Create New Detail : ' + ClassComponentCategoryName)
            $('#new_detail_class_standing_component_id').val(ClassStandingComponentId)
            $('#new_detail_class_id').val(classId)

        });

       
        $(".SaveNewDetail").click(function () {
            if($("#new_detail_description").val() != "" && $("#new_detail_perfect_score").val() != "" && $("#new_detail_perfect_score").val() != 0 && $("#new_detail_date").val() != ""){
                $.ajax({
                    url:"../../class_standing_component_detail/special_case/create",
                    type:'post',
                    data: 
                        { 
                            'class_standing_component_id': $("#new_detail_class_standing_component_id").val(),
                            'class_id': $("#new_detail_class_id").val(),
                            'description': $("#new_detail_description").val(),
                            'perfect_score': $("#new_detail_perfect_score").val(),
                            'date': $("#new_detail_date").val(),
                            '_token': $('input[name=_token]').val(),
                                                          
                        },
                    async:false
                }).done(function(data){
                    $("#newdetailspecialcasemodal").modal('hide');
                    swal("Saved!", "New Class Standing Component Detail Saved", "success");

                    
                    
                    $('.classStandingDetails').append('<tr id="tr_'+data.id+'"><td id="td_date_'+data.id+'">'+data.date+'</td><td id="td_description_'+data.id+'">'+data.description+'</td><td id="td_perfect_score_'+data.id+'">'+data.perfect_score+'</td><td>'

                    +'<button class="btn btn-sm btn-primary active" data-id="'+data.id+'" data-name="" data-description="'+data.description+'" data-date="'+data.date+'" data-perfect_score="'+data.perfect_score+'" data-toggle="modal" data-target="#scoreentrymodal"><span class="glyphicon glyphicon-list-alt"></span> Score Entry</button>'+
                                    '&nbsp;&nbsp;<button class="btn btn-sm btn-success active" data-id="'+data.id+'" data-cscd="'+data.description+'" data-toggle="modal" data-target="#cscdeditmodal"><span class="glyphicon glyphicon-pencil"></span> Edit</a></button>'+    
                                    '&nbsp;&nbsp;<button class="btn btn-sm btn-danger active" data-id="'+data.id+'" data-cscd="'+data.description+'" data-toggle="modal" data-target="#cscddeletemodal"><span class="glyphicon glyphicon-pencil"></span> Delete</a></button></td></tr>');
                });
                
            }
            else{
                swal('Some fields are not fill up')
                return false;
            }
        });
});