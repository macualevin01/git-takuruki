var hot;


$(document).ready(function () {
    ScoreEntryHandsonTable();
});

function ScoreEntryHandsonTable() {

    $('#scoreentryspecialcasemodal').on('shown.bs.modal', function (e) {
        $('#score_entry_excel').empty();
        var button =  $(e.relatedTarget)
        var ClassStandingComponentDetailId = button.data('id')
        var ClassStandingComponentDetailDescription = button.data('description')
        var ClassStandingComponentDetailDate = button.data('date')
        var ClassStandingComponentDetailPerfectScore = button.data('perfect_score')
        var classId = button.data('class_id')
        var ClassComponentCategoryName = button.data('name')
        var ClassGradingPeriodId = button.data('grading_period_id')

        var modal = $(this)
        $('.score_entry').text('Score Entry : ' + ClassComponentCategoryName)
        $('#description').val(ClassStandingComponentDetailDescription)
        $('#date').val(ClassStandingComponentDetailDate)
        $('#perfect_score').val(ClassStandingComponentDetailPerfectScore)
        $('#score_entry_class_id').val(classId)

        $.ajax({
            url:"../../class_standing_score_entry/dataJson",
            type:'get',
            data:{  
                    'class_standing_component_detail_id' : ClassStandingComponentDetailId
                },
            dataType: "json",
            async:false,
            success: function (data) { 
                studentJsonArray = data;
            } 
        });
        $.ajax({
            url:"../../class_record/ScoreEntryLockOrUnlock",
            type:'get',
            data:{ 
                'grading_period_id' : ClassGradingPeriodId,
                'class_id' : $('#gp_class_id').val()
            },
            dataType: "json",
            async:false,
            success: function (data){
                Security = data;
            }
        });

        // $.ajax({
        //     url:"../../attendance_remark/dataJson",
        //     type:'get',
        //     data:{ },
        //     dataType: "json",
        //     async:false,
        //     success: function (data) { 
        //         attendance_remark = data;
        //     }
        // });
        // $.ajax({
        //     url:"../../class_record/failOrPass",
        //     type:'get',
        //     data:{ 
        //         'grading_period_id' : GradingPeriodId,
        //         'class_id' : ClassId
        //     },
        //     dataType: "json",
        //     async:false,
        //     success: function (data){
        //         PassOrFail = data;
        //     }
        // });

        $.ajax({
            url:"../../class_standing_score_entry/class_record_pass_or_failed",
            type:'get',
            data:{  
                    'class_standing_component_detail_id' : ClassStandingComponentDetailId
                },
            dataType: "json",
            async:false,
            success: function (data) { 
                PassOrFail = data;
            } 
        });
        

        var data = studentJsonArray;
        
        var color_arr = PassOrFail;
        var security_data = Security;
        if(security_data == 1){

            sweetAlert("Oops...", "You are not allowed to do something!", "error");
        }
        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
          
            
        }
        function failRenderer(instance, td, row, col, prop, value, cellProperties,changes) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);

            // td.style.color = 'red';
            var perfect_score = parseInt(ClassStandingComponentDetailPerfectScore);
            var new_score_60_percent = perfect_score * .6 ;
            if(value >= new_score_60_percent)
            {
                td.style.color = '#000';
            }
            else
            {
                td.style.color = 'red';
            }
            
        }
        var container = document.getElementById("score_entry_excel");
        // if(ClassComponentCategoryName == 'Attendance'){
        //     hot = new Handsontable(container, {
        //         data: data,
        //         afterChange: function (change, source) {
        //             $.ajax({
        //                 url: "../../class_standing_score_entry/postupdateA",
        //                 data: {
        //                     'data': data,
        //                     '_token': $('input[name=_token]').val(), //returns all cells' data
        //                 }, 
        //                 dataType: 'json',
        //                 type: 'POST',
        //                 async:false
        //             });
        //         },
        //         colHeaders: ["ID","Student Id", "Last Name", "First Name", "Middle Name","", "Attendance"],
        //         rowHeaders: true,
        //         className: "htCenter htMiddle",
        //         height: 300,
        //         currentRowClassName: 'currentRow',
        //         currentColClassName: 'currentCol',
        //         colWidths:[50,100,100,100,100,80,100],
        //         columns: [{   
        //             data: 'id',
        //             readOnly: true 
        //         },
        //         {   
        //             data: 'student_no',
        //             readOnly: true 
        //         },
        //         {   
        //             data: 'last_name',
        //             readOnly: true  
        //         },
        //         { 
        //             data: 'first_name',
        //             readOnly: true  
        //         },
        //         { 
        //             data: 'middle_name',
        //             readOnly: true  
        //         },
        //         { 
                   
        //             readOnly: true  
        //         },
        //         {
        //             data:'attendance_remarks_code',
        //             type: 'handsontable',
        //             handsontable: {
        //                 colHeaders: false,
        //                 data: attendance_remark,
        //                 columns:[{data:'text'}]
        //             }
        //         }],

        //         beforeKeyDown: function (e) {
        //             var selection = hot.getSelected();

        //             //call a function that will check if the e.keyCode corresponds to numeric value
        //             var score_entry_column = selection[1];
        //             if (!((e.keyCode === 80 || e.keyCode === 76 || e.keyCode === 65 || e.keyCode === 13 || (e.keyCode >= 37 && e.keyCode <= 40)))){
        //                 Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
        //                 e.preventDefault();
        //             }
        //         }
        //     });
        // }else{
            hot = new Handsontable(container, {
                data: data,
                beforeChange: function (changes, source) {

                    //you need to have the perfect score
                    //compare the perfect to the new cell value

                    var perfect_score = parseInt(ClassStandingComponentDetailPerfectScore);
                    var new_score = parseInt(changes[0][3]);
                    var field = changes[0][1];
                    if(field == "score"){ 
                        if(new_score > perfect_score ){

                            sweetAlert("Oops...", "Score must be lesser than or equal to perfect score!", "error");
                            return false;
                        }
                    }
                },
                afterChange: function (change, source) {
                    $.ajax({
                        url: "../../class_standing_score_entry/postupdate",
                        data: {
                            'data': data,
                            'grading_period_id' : ClassGradingPeriodId,
                            'class_id' : $('#gp_class_id').val(),
                            '_token': $('input[name=_token]').val(), //returns all cells' data
                        }, 
                        dataType: 'json',
                        type: 'POST',
                        async:false
                    });
                },
                colHeaders: ["ID","Student Id", "Last Name", "First Name", "Middle Name", "Score"],
                className: "htCenter htMiddle",
                height: 1000,
                currentRowClassName: 'currentRow',
                currentColClassName: 'currentCol',
                fixedRowsTop: 3,
                fixedColumnsLeft: 4,
                // },
                // {
                //     data:'attendance_remarks_code',
                //     type: 'handsontable',
                //     handsontable: {
                //         colHeaders: false,
                //         data: attendance_remark,
                //         columns:[{data:'text'}]
                //     }
                // }],
                cells: function (row, col, prop ) {
                    var cellProperties = {};
                    // alert(data[2]['score']);
                    // alert(color_arr[col]['score']);
                    

                            // alert(row+" "+col)
             
                if(row != 0 && row >= 0){
                    if(col != 0 && col == 5){

                        if(color_arr[row]['score'] == 'failed'){
                            cellProperties.renderer = failRenderer;

                        }
                        // alert(row+" "+col)
                    }
                    else{

                    }
                    
                }
                else{
                    // cellProperties.renderer = successRenderer;
                }

                if(security_data == 1){
                    if (row > 0 || col > 0) {
                        cellProperties.editor = false;
                    }
                    if(col === 0 || col === 1 || col === 2 || col === 3 || col === 4 || col === 5) {
                        cellProperties.renderer = firstRowRenderer;
                    }
                }
                else{
                    
                    
                    if(col === 0 || col === 1 || col === 2 || col === 3 || col === 4) {
                        cellProperties.editor = false;
                        cellProperties.renderer = firstRowRenderer;
                    }
                }

                  return cellProperties;
                },
                beforeKeyDown: function (e) {
                    // var selection = hot.getSelected();

                    //call a function that will check if the e.keyCode corresponds to numeric value
                    // var score_entry_column = selection[1];
                    // if(score_entry_column == '6'){
                    //     if (!((e.keyCode === 80 || e.keyCode === 76 || e.keyCode === 65 || e.keyCode === 13 || e.keyCode === 8 || (e.keyCode >= 37 && e.keyCode <= 40)))){
                    //         Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                    //         e.preventDefault();
                    //     }
                    // }else{
                        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 )){
                            Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                            e.preventDefault();
                        }
                    // }
                }
            });            
        // }
    });
}