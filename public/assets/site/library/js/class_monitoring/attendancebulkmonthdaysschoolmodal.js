var hot;

$(document).ready(function () {
    AttendanceBulkDaysSchoolHandsonTable();
});

function AttendanceBulkDaysSchoolHandsonTable() {

    $('#attendancebulkdaysschoolmodal').on('shown.bs.modal', function (e) {
        $(".htCore tbody tr").remove();
        $(".attendance_bulk_days_school_modal").empty();
        $(".attendance_bulk_days_school_excel").empty();

        var button =  $(e.relatedTarget)
        var ClassificationId = button.data('classification_id')
        var ClassificationLevelId = button.data('classification_level_id')
        var TermId = button.data('term_id')
        var SectionId = button.data('section_id')
        var Level = button.data('level')
        var SectionName = button.data('section_name')
        var TermName = button.data('term_name')

        $("#edit_delete").attr('data-section_id',SectionId);
        $("#edit_delete").attr('data-term_id',TermId);
        modifyAttendance();

        var modal = $(this)
        $('.attendance_bulk_days_school_modal').append('Attendance '+Level+' '+SectionName+' '+TermName)
        $('#attendance_bulk_days_school_classification_id').val(ClassificationId)
        $('#attendance_bulk_days_school_classification_level_id').val(ClassificationLevelId)
        $('#attendance_bulk_days_school_term_id').val(TermId)
        $('#attendance_bulk_days_school_section_id').val(SectionId)

        $.ajax({
            url:"../../teachers_portal/class_monitoring/updateAttendanceDSBulk",
            type:'post',
            data:{
                    'term_id': $("#attendance_bulk_days_school_term_id").val(),
                    'classification_level_id': $("#attendance_bulk_days_school_classification_level_id").val(),
                    'section_id': $("#attendance_bulk_days_school_section_id").val(),
                    '_token': $('input[name=_token]').val(),
                },
            async:false
        });
// teachers_portal/class_monitoring/dataJsonStudentAttendanceDaysSchoolBulk
        $.ajax({
            url:"../../teachers_portal/class_monitoring/dataJsonStudentAttendanceBulk",
            type:'get',
            data:{  
                    'term_id' : TermId,
                    'section_id' : SectionId,
                    'classification_level_id' : ClassificationLevelId,
                },
            dataType: "json",
            async:false,
            success: function (data) { 
                StudentAttendanceArray = data;
            } 
        });

        // $.ajax({
        //     url:"../../attendance_remark/dataJson",
        //     type:'get',
        //     data:{ },
        //     dataType: "json",
        //     async:false,
        //     success: function (data) { 
        //         attendance_remark = data;
        //     }
        // });

        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
        }
        function firstDaysofSchoolRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#FFD08B';
            td.style.color = '#FFFFFF';
        }
        function firstPresentRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#1383A8';
            td.style.color = '#FFFFFF';
        }
        function firstAbsentRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#DD5044';
            td.style.color = '#FFFFFF';
        }
        function firstTardyRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#17A05D';
            td.style.color = '#FFFFFF';
        }
        var data = StudentAttendanceArray[0];
        var student_attendance_id = StudentAttendanceArray[1];

        var container = document.getElementById("attendance_bulk_excel");
        hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {

                //you need to have the perfect score
                //compare the perfect to the new cell value
                // alert(parseInt(perfect_score_arr[changes[0][1]]));
                var days_max = 31;
                var days_entry = parseInt(changes[0][3]);
                if(days_entry > days_max ){
                    sweetAlert("Oops...", "Days must be lesser than or equal to 31!", "error");
                    return false;
                }
                // if(new_score < new_score_60_percent ){
                //    $(this).find('td[class="htCenter"]').attr('style','color: red;');
                
                // }

            },
            afterChange: function (changes, source) {

                
                if(changes != null){
                    var rating_row = changes[0][0];
                    var rating_column = changes[0][1];
                    var student_id = student_attendance_id[rating_row][rating_column];
                    var rating = changes[0][3];
                    $.ajax({
                        url: "../../teachers_portal/class_monitoring/postStudentAttendanceBulk",
                        data: {
                            'id': student_id,
                            'rating': rating,
                            '_token': $('input[name=_token]').val(),
                        }, 
                        dataType: 'json',
                        type: 'POST',
                        async:false
                    });
                }
            },
            className: "htCenter htMiddle",
            height: 300,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            fixedColumnsLeft: 3,
            mergeCells: [
                {row: 0, col: 3, rowspan: 1, colspan: 4},
                {row: 0, col: 7, rowspan: 1, colspan: 4},
                {row: 0, col: 11, rowspan: 1, colspan: 4},
                {row: 0, col: 15, rowspan: 1, colspan: 4},
                {row: 0, col: 19, rowspan: 1, colspan: 4},
                {row: 0, col: 23, rowspan: 1, colspan: 4},
                {row: 0, col: 27, rowspan: 1, colspan: 4},
                {row: 0, col: 31, rowspan: 1, colspan: 4},
                {row: 0, col: 35, rowspan: 1, colspan: 4},
                {row: 0, col: 39, rowspan: 1, colspan: 4},
                {row: 0, col: 43, rowspan: 1, colspan: 4}
            ],
            cells : function(row, col, prop, td) {
                var cellProperties = {};

                if(row === 0 && col <= 42 || col <= 2 ) {
                    cellProperties.editor = false;
                    cellProperties.renderer = firstRowRenderer;
                }
                if(row != 0 && col == 3 || row != 0 &&  col == 7 || row != 0 &&  col == 11 || row != 0 &&  col == 15 || row != 0 &&  col == 19 || row != 0 &&  col == 23 || row != 0 &&  col == 27 || row != 0 &&  col == 31 || row != 0 &&  col == 35 || row != 0 &&  col == 39){
                	 cellProperties.renderer = firstDaysofSchoolRenderer;
                }
                if(col == 4 || col == 8 || col == 12 || col == 16 || col == 20 || col == 24 || col == 28 || col == 32 || col == 36 || col == 40){
                	 cellProperties.renderer = firstPresentRenderer;
                }
                if(col == 5 || col == 9 || col == 13 || col == 17 || col == 21 || col == 25 || col == 29 || col == 33 || col == 37 || col == 41){
                	 cellProperties.renderer = firstAbsentRenderer;
                }
                if(col == 6 || col == 10 || col == 14 || col == 18 || col == 22 || col == 26 || col == 30 || col == 34 || col == 38 || col == 42){
                	 cellProperties.renderer = firstTardyRenderer;
                }

                return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                var rating_column = selection[1];
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 )){
                        Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                        e.preventDefault();
                    }
                
            }
        });   
    });
}