$(document).ready(function(){
    $('#editStudentCurriculum').on('shown.bs.modal', function(e) {   

        var button =  $(e.relatedTarget)
        var StudentCurriculumId = button.data('student_curriculum_id')
        var StudentId = button.data('student_id')
        var LastName = button.data('last_name')
        var FirstName = button.data('first_name')
        var MiddleName = button.data('middle_name')
        var SuffixName = button.data('suffix_name')
        var CurriculumId = button.data('curriculum_id')
        var ClassificationId = button.data('classification_id')
        var Img = button.data('img')

        var modal = $(this)
        $('.edit_student_curriculum_modal').text('Edit Student Curriculum')
        $('#student_id').val(StudentId)
        $('#student_curriculum_id').val(StudentCurriculumId)
        $('#edit_classification_id').val(ClassificationId)
        // $("#edit_curriculum_id [value='']").removeAttr("selected","selected");
        selectListChange('edit_curriculum_id','../curriculum/dataJson',  { 'is_active':1 , 'classification_id': ClassificationId } ,'Please select a Curriculum')
        $("#edit_curriculum_id [value='"+CurriculumId+"']").attr("selected","selected");
        if(Img){

            $('#img').attr('src','../'+Img)
        }
        else{
            $('#img').attr('src','../assets/site/images/student-default.png')
        }
        $('#edit_enrolled_student_name').text(LastName+', '+FirstName+' '+MiddleName+' '+SuffixName)

        // $.ajax({
        //       url:"enroll_student/editdataJson",
        //       type:'get',
        //       data:
        //           {  
        //             'student_id' : StudentId
        //           },
        //       dataType: "json",
        //       async:false

        // }).done(function(data) {
            
        //     if(data.length > 0)
        //     {
        //         $.each( data, function( key, item ) {

        //             $("#edit_enrolled_student_payment_scheme").val(item.payment_scheme_id);
        //         });
        //     }
        // });

     // var payment_scheme_id = $("#edit_enrolled_student_payment_scheme").val();
     //    $.ajax({
     //        url:"payment_scheme/dataJson",
     //        type:'GET',
     //        data:
     //            {  
     //                'id': $("#payment_scheme_id").val(),
     //            },
     //        dataType: "json",
     //        async:false,
     //        success: function (data) 
     //        {    
                
     //            $("#edit_enrolled_student_payment_scheme").empty();
     //            $("#edit_enrolled_student_payment_scheme").append('<option value=""></option>');
     //            $.map(data, function (item) 
     //            {       
     //                    payment_scheme_name = item.text;

     //                    $("#edit_enrolled_student_payment_scheme").append('<option value="'+item.value+'">'+payment_scheme_name+'</option>');
     //            });
     //        }  
     //    });

        
    });



        $(".EditStudentCurriculum").click(function () {
            $.ajax({
                url:"student_curriculum/edit",
                type:'post',
                data:{ 
                        'id': $("#student_curriculum_id").val(),
                        'curriculum_id': $("#edit_curriculum_id").val(),
                        'classification_id': $("#edit_classification_id").val(),
                        '_token': $('input[name=_token]').val(),
                    },
                async:false
            });
            $("#editEnrolledStudent").modal('hide');
            swal("Edited!", "Successfully Edit the Student Curriculum", "success"); 
            $('#table').dataTable().fnReloadAjax("student_curriculum/data?classification_id="+$("#edit_classification_id").val());

        });

        $(".DeleteEnrolledStudent").click(function () {
            $.ajax({
                url:"enroll_student/postDeletedataJson",
                type:'post',
                data:{ 
                        'id': $("#enrollment_id").val(),
                        'club_id': $("#edit_enrolled_student_club").val(),
                        'semester_level_id': $("#edit_enrolled_semester_level_club").val(),
                        'term_id': $("#edit_enrolled_term_club").val(),
                        '_token': $('input[name=_token]').val(),
                    },
                async:false
            });
            $("#editEnrolledStudent").modal('hide');
            swal("Deleted", "Successfully Delete", "error");  
            $('#table').dataTable().fnReloadAjax("enroll_student/data?level="+$("#classification_level_id").val()+"&term_id="+$("#term_id").val()+"&semester_level_id="+$("#semester_level_id").val());

        });

    });
// });