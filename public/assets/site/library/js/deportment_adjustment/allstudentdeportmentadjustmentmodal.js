var hot;

$(document).ready(function () {
    AllStudentDeportmentHandsonTable();
});

function AllStudentDeportmentHandsonTable() {

    $('#allstudentdeportmentadjustmentmodal').on('shown.bs.modal', function (e) {
            $(".all_deportment_record").text('');
            // $("#all_student_deportment_adjustment_excel").empty();
            var button =  $(e.relatedTarget)

            var ClassificationLevelId = button.data('classification_level_id')
            var TermId = button.data('term_id')
            var SemesterLevelId = button.data('semester_level_id')
            var SectionId = button.data('section_id')
            var GradingPeriodId = button.data('grading_period_id')
            var Level = button.data('level')
            var SectionName = button.data('section_name')
            var SemesterName = button.data('semester_name')
            var TermName = button.data('term_name')
            var GradingPeriodName = button.data('grading_period_name')

            var modal = $(this)

            $(".all_deportment_record").text(Level+' '+SectionName+'                           '+SemesterName+' '+GradingPeriodName+' ('+TermName+')');
            
            $.ajax({
                url:"../deportment_adjustment_data/ScoreEntryLockOrUnlock",
                type:'get',
                // data:{ 
                //     'classification_level_id' : ClassificationLevelId,
                //     'grading_period_id' : GradingPeriodId,
                //     'section_id' : SectionId,
                //     'term_id' : TermId,
                //     'semester_level_id' : SemesterLevelId,
                // },
                dataType: "json",
                async:false,
                success: function (data){
                    Security = data;
                }
            });

            // $.ajax({
            //         url:"../teachers_portal/deportment_adjustment_data/postAllDeportmentCreate",
            //         type:'post',
            //         data: 
            //             { 
                            
            //                 'classification_level_id' : ClassificationLevelId,
            //                 'grading_period_id' : GradingPeriodId,
            //                 'section_id' : SectionId,
            //                 'term_id' : TermId
            //                 'semester_level_id' : SemesterLevelId,
            //                 '_token': $('input[name=_token]').val(),

                                                          
            //             },
            //         async:false
            // });



            $.ajax({
                url:"../deportment_adjustment_data/dataAllDeportmentJsonEdit",
                type:'post',
                data:{  
                        'grading_period_id' : GradingPeriodId,
                        'classification_level_id' : ClassificationLevelId,
                        'term_id' : TermId,
                        'section_id' : SectionId,
                        'semester_level_id' : SemesterLevelId,
                        '_token': $('input[name=_token]').val(),
                    },
                dataType: "json",
                async:false,
                success: function (data){ 
                    ScoreAllDeportmentEntryJson = data;
                } 
            });
            // console.log(ScoreAllDeportmentEntryJson);
            var data = ScoreAllDeportmentEntryJson[0];
            var score_entry_all_id_arr = ScoreAllDeportmentEntryJson[1];
            var student_id_arr = ScoreAllDeportmentEntryJson[2];
            var security_data = Security;
            if(security_data == 1){

                sweetAlert("Oops...", "You are not allowed to do something!", "error");
                // return false;
            }

            function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
            }
            function firstRowColorRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#007500';
            td.style.color = 'white';
            }

            function firstRowDeportmentColorRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#9ECDF2';
            td.style.color = 'black';
            }
            function firstRowNamesColorRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#F5BD00';
            td.style.color = 'black';
            }
            //     function colorRenderer(instance, td, row, col, prop, value, cellProperties) {

            //         Handsontable.renderers.TextRenderer.apply(this, arguments);

            //                 // get reference to data
            //         var data = instance.getData();
            //         // grab the value of the current row at the column we want to compare to
            //         var valueAtC = data[row][2];
            //         if (valueAtC > value) {
            //             td.style.backgroundColor = 'red';
            //         } else {
            //             td.style.backgroundColor = 'green';
            //         }

            //         return td;
            //     };


            var container = document.getElementById("all_student_deportment_adjustment_excel");
            hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {

                //you need to have the perfect score
                //compare the perfect to the new cell value
                // alert(parseInt(perfect_score_arr[changes[0][1]]));
                var perfect_score = 100;
                var new_score = parseInt(changes[0][3]);
                var sixty_percent = parseInt(.60);
                var new_score_60_percent = perfect_score * .6 ;
                var low_score = 70;
                if(new_score > perfect_score ){
                    sweetAlert("Oops...", "Score must be lesser than or equal to 100!", "error");
                    return false;
                }
                if(low_score > new_score ){
                    sweetAlert("Oops...", "Score must be greater than or equal to 70!", "error");
                    return false;
                }
                // if(new_score < new_score_60_percent ){
                //    $(this).find('td[class="htCenter"]').attr('style','color: red;');
                
                // }

            },
            afterChange: function (changes, source) {

                if(changes != null){
                    var score_entry_row = changes[0][0];
                    var score_entry_column = changes[0][1];
                    var score_entry_id = score_entry_all_id_arr[score_entry_row][score_entry_column];
                    var student_id = student_id_arr[score_entry_row][score_entry_column];
                    var new_score = changes[0][3];
                    // alert(student_id);
                        if(new_score == ""){
                            $.ajax({
                                url: "../deportment_adjustment_data/postUpdateAdjustmentGradeDeportment",
                                data: {
                                    'id': score_entry_id, 
                                    'grade': new_score,
                                    'student_id' : student_id,
                                    'term_id' : TermId,
                                    'grading_period_id' : GradingPeriodId,
                                    '_token': $('input[name=_token]').val(),
                                },
                                dataType: 'json',
                                type: 'POST',
                                async:false
                            });
                        }
                        else{
                           $.ajax({
                                url: "../deportment_adjustment_data/postUpdateAdjustmentGradeDeportment",
                                data: {
                                    'id': score_entry_id, 
                                    'grade': new_score,
                                    'student_id' : student_id,
                                    'term_id' : TermId,
                                    'grading_period_id' : GradingPeriodId,
                                    '_token': $('input[name=_token]').val(),
                                },
                                dataType: 'json',
                                type: 'POST',
                                async:false
                            }); 
                           
                           
                        }

                    
                }
            },
            className: "htCenter htMiddle",
            height: 450,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 1,
            fixedColumnsLeft: 5,

            cells: function (row, col, prop, changes) {
                var cellProperties = {};
                if(security_data == 1){
                    if (row > 0 && col > 0) {
                        cellProperties.editor = false;
                        cellProperties.renderer = firstRowRenderer; 
                    }
                }
                else{
                    
                    
                    if(row === 0 || col === 0 || col === 1 || col === 2 || col === 3 || col === 4) {
                        cellProperties.editor = false;
                        cellProperties.renderer = firstRowColorRenderer;
                    }
                    if(row == 0 && col >= 5){
                        
                        cellProperties.renderer = firstRowDeportmentColorRenderer;
                    }
                    if(row == 0 && col <= 4){
                        
                        cellProperties.renderer = firstRowNamesColorRenderer;
                    }
                }
                



              return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode == 9) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 51   || e.keyCode === 117 )){
                        Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                        e.preventDefault();
                    }
                
            }

        });
    });     
};