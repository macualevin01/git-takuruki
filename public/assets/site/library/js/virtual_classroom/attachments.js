function attachFile(data_id)
{
	$('#attach_file_'+data_id).trigger('click'); 

 	$('#attach_file_'+data_id).change(function(e){
 		var file = this.files[0];
	    uploadFile(file,data_id);

 		$('#attach_file_'+data_id).val('');
 	})
}

function attachFileWithIndicator(data_id)
{
	$('#attach_file_'+data_id).trigger('click'); 

 	$('#attach_file_'+data_id).change(function(e){
 		var file = this.files[0];
	    uploadFileWithIndicator(file,data_id);

 		$('#attach_file_'+data_id).val('');
 	})
}

function postAttachFile(){
	$('#post_attach_file').trigger('click'); 

 	$('#post_attach_file').change(function(e){
 		var file = this.files[0];
 		// console.log(file);

 		$.map(this.files,function(file){
 			// console.log(file);
	    	post_image_attach_arr.push(file);
 			var reader = new FileReader();
    
		    reader.onload = function(e) {
		    	// console.log(e.target);
		    	// console.log(e.target.result);

		    	$('#post_attachment_div').append('<div class="col-md-4" style="text-align: center;"><img src="'+e.target.result+'" height="200"></div>');
		    }
		    
		    reader.readAsDataURL(file); // convert to base64 string
 		})

 		console.log(post_image_attach_arr);

 		$('#post_attach_file').val('');
 	})
}

function removeAttached(data_id)
{
	$.ajax({
		url: '../classwork/remove_attachment',
		type: 'post',
		data: {
			id:data_id,
			_token: $('#_token').val(),
		}
	}).done(function(data){
		$('#attachment_'+data_id).remove();
	})
}

$('#addLinkModal').on('shown.bs.modal',function(e){
	var data_id = $(e.relatedTarget).data('id');

	$('#attach_link_question_id').val(data_id);
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$('#link').keyup(function(){
	if($(this).val())
	{
		$('#save_attach_link').removeAttr('disabled');
	}else{
		$('#save_attach_link').attr('disabled','disabled');
	}
});

$('#save_attach_link').click(function(){

	if(!isValidURL($('#link').val())){
		swal('Error','Invalid link. Please try another link.','error');

		return false;
	}

	$('#save_attach_link').attr('disabled','disabled');
	$('#save_attach_link').text('attaching...');
	$.ajax({
		url: '../classwork/attach_link',
		type: 'post',
		data: {
			class_work_question_id: $('#attach_link_question_id').val(),
			title_link: $('#title_link').val(),
			link: $('#link').val(),
			_token: $('#_token').val(),
		}
	}).done(function(data){
		$('#attachment_div_'+$('#attach_link_question_id').val()).append(data);
		$('#title_link').val('');
		$('#link').val('');
		$('#save_attach_link').removeAttr('disabled');
		$('#save_attach_link').text('attach');
		$('#addLinkModal').modal('hide');
	})
});

$('.tab_panel_body').on('click','.clickable_link',function(){
	window.open($(this).data('link'),'_blank');
});

function isValidURL(string) {
  var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
  return (res !== null)
};


// #functions
function uploadFile(file,id)
{
	if(file == undefined)
	{
		return;
	}
	
	var formData = new FormData();
	formData.append('id', id);
	formData.append('file', file);
	formData.append('_token', $('#_token').val());
	$.ajax({
	    url: '../classwork/upload_file',  //Server script to process data
	    type: 'POST',
	    data: formData,
	    contentType: false,
	    processData: false,
	    //Ajax events
	    success: function(data){
	    	if(data.status == false)
	    	{
	    		$('#attachment_div_'+id).append('<p style="color:red;" id="attachment_error_'+id+'">The extension type of file "'+data.file_name+'" is not allowed.</p>');
	    	}else{
			    $('#attachment_error_'+id).remove();
			   	$('#attachment_div_'+id).append(data);
	    	}
	    }
	});
}

// #functions
function uploadFileWithIndicator(file,id)
{
	if(file == undefined)
	{
		return;
	}

	var formData = new FormData();
	formData.append('id', id);
	formData.append('file', file);
	formData.append('_token', $('#_token').val());
	// $.ajax({
	//     url: '../classwork/upload_file_with_indicator',  //Server script to process data
	//     type: 'POST',
	//     data: formData,
	//     contentType: false,
	//     processData: false,
	//     //Ajax events
	//     success: function(data){
	//     	if(data.status == false)
	//     	{
	//     		$('#attachment_div_'+id).append('<p style="color:red;" id="attachment_error_'+id+'">The extension type of file "'+data.file_name+'" is not allowed.</p>');
	//     	}else{
	// 		    $('#attachment_error_'+id).remove();
	// 		   	$('#attachment_div_'+id).append(data);
	//     	}
	//     }
	// });

	var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
	ajax.class_work_question_id = id;
    ajax.open("POST", '../classwork/upload_file_with_indicator'); // http://www.developphp.com/video/JavaScript/File-Upload-Progress-Bar-Meter-Tutorial-Ajax-PHP
      //use file_upload_parser.php from above url
    ajax.send(formData);
}


function _(el) {
  return document.getElementById(el);
}

function progressHandler(event) {
    $('.error_upload_message').remove();
  // _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
  var percent = (event.loaded / event.total) * 100;
  $('#progressBar').show();
  _("progressBar").value = Math.round(percent);
  _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
}

function completeHandler(event) {
    $('#progressBar').hide();
    $('#attachment_div_'+event.target.class_work_question_id).append(event.target.responseText);
    $('#status').empty();
  // _("attachment_con").innerHTML = event.target.responseText;
  _("progressBar").value = 0; //wil clear progress bar after successful upload
}

function errorHandler(event) {
    $('#progressBar').hide();
  _("status").innerHTML = "Upload Failed";
}

function abortHandler(event) {
    $('#progressBar').hide();
  _("status").innerHTML = "Upload Aborted";
}


function addPosting()
{
	$('#post_content').text(editor.getData());
	
	var formData = new FormData($('#postForm')[0]);
	$.map(post_image_attach_arr,function(image,i){
		console.log(image);
		formData.append('image_'+i, image);

	});
	
	formData.append('class_id', $('#class_id').val());
	formData.append('no_of_image', post_image_attach_arr.length);
	$.ajax({
	    url: '../add_post',  //Server script to process data
	    type: 'POST',
	    data: formData,
	    contentType: false,
	    processData: false,
	    //Ajax events
	    success: function(data){
	    	post_image_attach_arr = [];

	    	$('#addPost').modal('hide');
	    	$('#postForm')[0].reset();
	    	$('#post_attachment_div').empty();
	    	getPost();
	    	// if(data.status == false)
	    	// {
	    	// 	$('#attachment_div_'+id).append('<p style="color:red;" id="attachment_error_'+id+'">The extension type of file "'+data.file_name+'" is not allowed.</p>');
	    	// }else{
			   //  $('#attachment_error_'+id).remove();
			   // 	$('#attachment_div_'+id).append(data);
	    	// }
	    }
	});
}
