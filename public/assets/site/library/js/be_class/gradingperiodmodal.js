$(document).ready(function() {
	$('#gradingperiodmodal').on('show.bs.modal', function(e) {   
  
	    var button =  $(e.relatedTarget)
	    var classId = button.data('id')
        var Level = button.data('level')
	    var SectionName = button.data('section_name')
        var Name = button.data('name')
        var TermId = button.data('term_id')
        var ClassificationLevelId = button.data('classification_level_id')
        var SectionId = button.data('section_id')
        var SemesterLevelId = button.data('semester_level_id')
        var SubjectId = button.data('subject_id')
	    var ClassificationId = button.data('classification_id')

	    var modal = $(this)
        $('.grading_period').text('Grading Period - ' + Level +' '+SectionName)
	    $('.grading_period_subject').text(Name)
        $('#gp_section_name').val(SectionName)
        $('#gp_level').val(Level)
        $('#gp_class_id').val(classId)
        $('#gp_term_id').val(TermId)
        $('#gp_section_id').val(SectionId)
        $('#gp_classification_level_id').val(ClassificationLevelId)
        $('#gp_semester_level_id').val(SemesterLevelId)
        $('#gp_subject_id').val(SubjectId)
	    $('#gp_classification_id').val(ClassificationId)

	    gradingperiodtable.fnDraw();
	});

    gradingperiodtable = $('#gradingperiodtable').dataTable( {
        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "bProcessing": true,
        "bServerSide": true,
        "bStateSave": true,
        "sAjaxSource": "../../teachers_portal/be_class/grading_period_data",
        "fnDrawCallback": function ( oSettings ) {
        },
        "fnServerParams": function(aoData){
            aoData.push(
                { "name":"class_id", "value": $("#gp_class_id").val() },
                { "name":"term_id", "value": $("#gp_term_id").val() },
                { "name":"section_id", "value": $("#gp_section_id").val() },
                { "name":"classification_level_id", "value": $("#gp_classification_level_id").val() },
                { "name":"semester_level_id", "value": $("#gp_semester_level_id").val() },
                { "name":"subject_id", "value": $("#gp_subject_id").val() },
                { "name":"level", "value": $("#gp_level").val() },
                { "name":"section_name", "value": $("#gp_section_name").val() },
                { "name":"classification_id", "value": $("#gp_classification_id").val() }
            );
        }
    });
});