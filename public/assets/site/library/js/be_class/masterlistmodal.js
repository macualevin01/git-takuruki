var hot;

$(document).ready(function () {

    AllSpecialStudentChineseHandsonTable();
      
});


function AllSpecialStudentChineseHandsonTable(){
    $('#masterlistmodal').on('shown.bs.modal', function(e) {   
        $('#all_special_student_chinese_excel').empty();
        var button =  $(e.relatedTarget)
        var section_id = $(e.relatedTarget).data('section_id');
        var classification_level_id = $(e.relatedTarget).data('level_id');
        $("#master_list_section_id").val(section_id);
        $("#master_list_level_id").val(classification_level_id);
        var Id = button.data('id')
        var Level = button.data('level')
        var SectionName = button.data('section_name')
        var Name = button.data('name')
        var SubjectId = button.data('subject_id')

        var modal = $(this)
        $('.master_list').text('Master List - ' + Level +' '+SectionName+' ('+Name+')')
        $('#masterlist_id').val(Id)
        $('#masterlist_subject_id').val(SubjectId)

        
        //code for insert data automatically


        $.ajax({
                url:"../../teachers_portal/getSpecialStudentChineseSubject",
                type:'get',
                data:{  

                        'class_id': $("#masterlist_id").val(),
                        'section_id': $("#master_list_section_id").val(),
                        'subject_id': $("#masterlist_subject_id").val(),
                        'classification_level_id': $("#master_list_level_id").val(),
                        '_token': $('input[name=_token]').val(),
                    },
                dataType: "json",
                async:false,
                success: function (data){ 
                    SetSpecialGradeEntryJson = data;
                } 
        });
        $.ajax({
            url:"../../teachers_portal/getNoSpecialStudentChineseSubject",
            type:'get',
            data:{ 
                'class_id': $("#masterlist_id").val(),
                'section_id': $("#master_list_section_id").val(),
                'subject_id': $("#masterlist_subject_id").val(),
                'classification_level_id': $("#master_list_level_id").val(),
                '_token': $('input[name=_token]').val(),
            },
            dataType: "json",
            async:false,
            success: function (data){
                PassOrFail = data;
            }
        });
            var data = SetSpecialGradeEntryJson[0];
            var special_student_grade_id_arr = SetSpecialGradeEntryJson[1];
            var color_arr = PassOrFail[0];

            function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            // td.style.background = '#E6E6E6';
            }
            function NoRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            // td.style.color = 'red';
            }

            function YesRenderer(instance, td, row, col, prop, value, cellProperties, changes) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);

            // if(value === "YES")
            // {

            //     td.style.color = 'blue';

            // }
            // else
            // {
            //     td.style.color = 'red';
            // }
            }
        var container = document.getElementById("all_special_student_chinese_excel");
        hot = new Handsontable(container, {
        data: data,
        className: "htCenter htMiddle",
        height: 500 ,
        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        fixedRowsTop: 1,
        columns: [
          {},
          {},
          {},
          {},
          {},
          {
            type: 'dropdown',
            source: ['YES', 'NO']
          }
        ],  
            cells: function (row, col, prop ) {
                var cellProperties = {};

                // alert(row+" "+col);
                // alert(color_arr[row][col]);
                if(row != 0 && row >= 1){
                    if(col != 0 && col > 4 ){

                        if(color_arr[row][col] == 'YES'){
                            if(row != 0 && col >= 0 ){
                                 cellProperties.renderer = YesRenderer;
                            }

                        }
                        else{
                            cellProperties.renderer = NoRenderer;

                        }
                        // alert(row+" "+col)
                    }
                    else{

                    }
                    
                }
                else{
                    // cellProperties.renderer = successRenderer;
                }
                
                // if(data[0][col] == 'Attendance'){
                //     if(col === col && row >= 3 ){
                //         return {
                //             type: 'handsontable',
                //             handsontable: {
                //                 colHeaders: false,
                //                 data: attendance_remark,
                //                 columns:[{data:'text'}]
                //             }
                //         }
                //     }
                // }

                if(row === 0 || col === 0 || col === 1 || col === 2 || col === 3 || col === 4) {
                    cellProperties.editor = false;
                }
                if (row === 0 || col === 0) {
                    cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                
                

              return cellProperties;
            },
            beforeChange: function (changes, source) {
                var cellProperties = {};

            },
            afterChange: function (changes, source) {
            var cellProperties = {};


                if(changes != null){
                    var student_special_grade_entry_row = changes[0][0];
                    var student_special_grade_entry_column = changes[0][1];
                    var student_special_grade_entry_id = special_student_grade_id_arr[student_special_grade_entry_row][student_special_grade_entry_column];
                    var student_special_grade_remark = changes[0][3];
                    // var attendance_remark = changes[0][3];
                    // if(data[0][score_entry_column] == 'Attendance'){
                        $.ajax({
                            url: "../../teachers_portal/postUpdateSpecialStudentChineseSubject",
                            data: {
                                'id': student_special_grade_entry_id, 
                                'student_special_grade_remark': student_special_grade_remark,
                                '_token': $('input[name=_token]').val(),
                            }, 
                            dataType: 'json',
                            type: 'POST',
                            async:false
                        });

                    
                }
        
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
    
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 9 || e.keyCode === 51   || e.keyCode === 117 )){
                        Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                        e.preventDefault();
                    }
                
            },
        });
    });
}
        