$(document).ready(function(){
    $('#newinterventionmodal').on('show.bs.modal', function(e) {   

        var button =  $(e.relatedTarget)
        var StudentId = button.data('id')
        var ClassId = button.data('class_id')
        var TermId = button.data('term_id')
        var StudentName = button.data('student_name')
        var InterventionClassificationId = button.data('classification_id')

        var modal = $(this)
        $('.new_intervention_modal').text('Create Intervention')
        $('#intervention_student_id').val(StudentId)
        $('#new_intervention_class_id').val(ClassId)
        $('#intervention_term_id').val(TermId)
        $('#student_name').text(StudentName)
        $('#intervention_classification_id').val(InterventionClassificationId)

        $.ajax({
            url:"../../grading_period/dataJsonGradingPeriodIntervention",
            type:'GET',
            data:
                {  
                    'class_id': $("#new_intervention_class_id").val(),
                },
            dataType: "json",
            async:false,
            success: function (data) 
            {    
                
                $("#itervention_grading_period_id").empty();
                $("#itervention_grading_period_id").append('<option value=""></option>');
                $.map(data, function (item) 
                {      
                    if(item.value <= 3)
                    {
                        $("#itervention_grading_period_id").append('<option value="'+item.value+'">'+item.text+'</option>');
                    }
                    else{
                        $("#itervention_grading_period_id").append('<option value="'+item.value+'">'+item.text+' '+item.description+'</option>');
                    }
                });
            }  

        });

    });

    $(".SaveIntervention").click(function () {
        $.ajax({
            url:"../../teachers_portal/student_intervention/postCreate",
            type:'post',
            data:{ 
                    'teacher_comment': $("#teacher_comment").val(),
                    'action_taken_id': $("#action_taken_id").val(),
                    'grading_period_id': $("#itervention_grading_period_id").val(),
                    'student_id': $("#intervention_student_id").val(),
                    'class_id': $("#new_intervention_class_id").val(),
                    'term_id': $("#intervention_term_id").val(),
                    'classification_id': $("#intervention_classification_id").val(),
                    '_token': $('input[name=_token]').val(),
                },
            async:false
        });
        $("#newinterventionmodal").modal('hide');
        $("#studentinterventionmodal").modal('hide');
        swal("Saved!", "Successfully Save Intervention", "success"); 
    });
});