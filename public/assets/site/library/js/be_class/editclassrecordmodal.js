var hot;

$(document).ready(function () {
    EditClassRecordHandsonTable();
});

function EditClassRecordHandsonTable() {

    $('#editclassrecordmodal').on('shown.bs.modal', function (e) {
            $('#edit_class_record_excel').empty();
            var button =  $(e.relatedTarget)
            var GradingPeriodId = button.data('id')
            var ClassId = button.data('class_id')
            var TermId = button.data('term_id')
            var ClassificationLevelId = button.data('classification_level_id')
            var SectionId = button.data('section_id')
            var level = button.data('level')
            var section_name = button.data('section_name')
            var GradingPeriodName = button.data('grading_period_name')
            var GradingDescription = button.data('description')

            var modal = $(this)
            $('.edit_class_record').text('Edit All Class Component Record : ' + GradingPeriodName+' '+GradingDescription+' ('+level+' '+section_name+')')
            $('#grading_period_id_class_record').val(GradingPeriodId)
            $('#class_id_class_record').val(ClassId)
            $('#section_id_class_record').val(SectionId)
            $('#classification_level_id_class_record').val(ClassificationLevelId)
            $('#term_id_class_record').val(TermId)

            $(this).find('#viewingClassRecord').attr('data-id', ClassId);
            $(this).find('#viewingClassRecord').attr('data-grading_period_id', GradingPeriodId);

        $.ajax({
            url:"../../class_record/dataJsonEdit",
            type:'get',
            data:{  
                    'grading_period_id' : GradingPeriodId,
                    'class_id' : ClassId
                },
            dataType: "json",
            async:false,
            success: function (data){ 
                ScoreEntryJson = data;
            } 
        });

        // $.ajax({
        //     url:"../../attendance_remark/dataJson",
        //     type:'get',
        //     data:{ },
        //     dataType: "json",
        //     async:false,
        //     success: function (data){ 
        //         attendance_remark = data;
        //     }
        // });
        
        $.ajax({
            url:"../../class_record/failOrPassEditClassRecord",
            type:'get',
            data:{ 
                'grading_period_id' : GradingPeriodId,
                'class_id' : ClassId
            },
            dataType: "json",
            async:false,
            success: function (data){
                PassOrFail = data;
            }
        });

        $.ajax({
            url:"../../class_record/ScoreEntryLockOrUnlock",
            type:'get',
            data:{ 
                'grading_period_id' : GradingPeriodId,
                'class_id' : ClassId
            },
            dataType: "json",
            async:false,
            success: function (data){
                Security = data;
            }
        });
        

        var data = ScoreEntryJson[0];
        var score_entry_id_arr = ScoreEntryJson[1];
        var perfect_score_arr = ScoreEntryJson[2];
        var security_data = Security;
        if(security_data == 1){

            sweetAlert("Oops...", "You are not allowed to do something!", "error");
            // return false;
        }
        var color_arr = PassOrFail;
        function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.background = '#E6E6E6';
          
            
        }
        function failRenderer(instance, td, row, col, prop, value, cellProperties,change) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            
            var perfect_score = parseInt(perfect_score_arr[prop]);
            var new_score_60_percent = perfect_score * .6 ;
            if(value >= new_score_60_percent)
            {
                td.style.color = '#000';
            }
            else
            {   
                td.style.color = 'red';
            }
            
        }
        function successRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.color = '#000';
            
            
        }
        var container = document.getElementById("edit_class_record_excel");
        hot = new Handsontable(container, {
            data: data,
            beforeChange: function (changes, source) {
                var cellProperties = {};

                //you need to have the perfect score
                //compare the perfect to the new cell value
                // alert(parseInt(perfect_score_arr[changes[0][1]]));
                var perfect_score_column = changes[0][1];
                var perfect_score = parseInt(perfect_score_arr[perfect_score_column]);
                var new_score = parseInt(changes[0][3]);
                var new_score_60_percent = perfect_score * .6 ;
                if(new_score > perfect_score ){
                    sweetAlert("Oops...", "Score must be lesser than or equal to perfect score!", "error");
                    return false;
                }
            },
                
            afterChange: function (changes, source) {



                if(changes != null){
                    var score_entry_row = changes[0][0];
                    var score_entry_column = changes[0][1];
                    var score_entry_id = score_entry_id_arr[score_entry_row][score_entry_column];
                    var new_score = changes[0][3];
                    // var attendance_remark = changes[0][3];
                    // if(data[0][score_entry_column] == 'Attendance'){
                    //     $.ajax({
                    //         url: "../../class_standing_score_entry/postUpdateAttendance",
                    //         data: {
                    //             'id': score_entry_id, 
                    //             'attendance_remark': attendance_remark,
                    //             '_token': $('input[name=_token]').val(),
                    //         }, 
                    //         dataType: 'json',
                    //         type: 'POST',
                    //         async:false
                    //     });
                    // }else{
                        if(new_score == ""){

                        }
                        else{
                           $.ajax({
                                url: "../../class_standing_score_entry/postUpdateScoreEntry",
                                data: {
                                    'id': score_entry_id, 
                                    'score': new_score,
                                    'grading_period_id' : GradingPeriodId,
                                    'class_id' : ClassId,
                                    '_token': $('input[name=_token]').val(),
                                },
                                dataType: 'json',
                                type: 'POST',
                                async:false
                            }); 
                           

                            
                           
                        }
                        
                    // }
                    
                }
            },
            className: "htCenter htMiddle",
            height: 1000,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            fixedRowsTop: 3,
            fixedColumnsLeft: 4,
            //      mergeCells: [
            //     {row: 0, col: 8, rowspan: 1, colspan: 3},
            //     {row: 0, col: 9, rowspan: 1, colspan: 3},
            //     {row: 0, col: 10, rowspan: 1, colspan: 3}
            // ],
            cells: function (row, col, prop ) {
                var cellProperties = {};

                // alert(row+" "+col);
                if(row != 0 && row >= 3){
                    if(col != 0 && col >= 5){

                        if(color_arr[row][col] == 'failed'){
                            cellProperties.renderer = failRenderer;

                        }
                        // alert(row+" "+col)
                    }
                    else{

                    }
                    
                }
                else{
                    // cellProperties.renderer = successRenderer;
                }
                
                // if(data[0][col] == 'Attendance'){
                //     if(col === col && row >= 3 ){
                //         return {
                //             type: 'handsontable',
                //             handsontable: {
                //                 colHeaders: false,
                //                 data: attendance_remark,
                //                 columns:[{data:'text'}]
                //             }
                //         }
                //     }
                // }
                if(security_data == 1){
                    if (row > 0 && col > 0) {
                        cellProperties.editor = false;
                    }
                }
                else{
                    
                    
                    if(row === 0 || row === 1|| row === 2 || col === 0 || col === 1 || col === 2 || col === 3 || col === 4) {
                        cellProperties.editor = false;
                    }
                    if (row === 0 || row === 1|| row === 2 || col === 0) {
                        cellProperties.renderer = firstRowRenderer; // uses function directly
                    }
                }
                

              return cellProperties;
            },
            beforeKeyDown: function (e) {
                var selection = hot.getSelected();
                //call a function that will check if the e.keyCode corresponds to numeric value
                // var score_entry_column = selection[1];
               
                
                    if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode === 13 || e.keyCode === 8  || e.keyCode === 9 || e.keyCode === 51   || e.keyCode === 117 )){
                        Handsontable.dom.stopImmediatePropagation(e); // remove data at cell, shift up
                        e.preventDefault();
                    }
                
            },
        });
    });
    $("#classrecordmodal").draggable({
        handle: ".modal-header"
    });
}

function callReport(reportId)
{
  var url = $("#"+reportId).data('url');
  var class_id = $("#class_id_class_record").val();
  var grading_period_id  = $("#grading_period_id_class_record").val();
  var section_id = $('#section_id_class_record').val();
  var classification_level_id = $('#classification_level_id_class_record').val();
  var term_id = $('#term_id_class_record').val();
            
  url = url +"?class_id="+class_id+"&grading_period_id="+grading_period_id+"&section_id="+section_id+"&classification_level_id="+classification_level_id+"&term_id="+term_id;
  
  window.open(url);
}