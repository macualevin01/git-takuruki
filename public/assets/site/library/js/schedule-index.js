


//this function will populate the time header 
function populateTimeHeader(timeJsonArray,name){
  $("#MainDiv").append('<div class="div-table-row"  id="TimeHeaderRow"></div>');
  $("#TimeHeaderRow").append('<div class="div-table-col-sec">'+name+'</div>');
  $(jQuery.parseJSON(JSON.stringify(timeJsonArray))).each(function() {  
          var timeStart = this;
          var timeEnd = getTimeByOrderNo(timeStart.order_no+1, timeJsonArray);
          if(timeEnd != null && timeStart.id > 62 && timeStart.id < 242){
          // alert(id+time);
              $("#TimeHeaderRow").append('<div class="div-table-col-time" id="TimeStart'+timeStart.id+'">'+timeStart.time+' '+timeStart.time_session+'</br>'+timeEnd.time+' '+timeEnd.time_session+'</div>');
          }
  });
}


function getTimeByOrderNo(orderNo, timeJsonArray)
{
  var time = null;

    $(jQuery.parseJSON(JSON.stringify(timeJsonArray))).each(function() {
        if(orderNo == this.order_no){  
          time = this; 
        }
    });

  return time;
}


function getTimeById(timeId, timeJsonArray)
{
  var time = null;

   $(jQuery.parseJSON(JSON.stringify(timeJsonArray))).each(function() {
              if(timeId == this.id){  
                time = this; 
            }
    });

   return time;
}


function populateIndexSchedule(sectionJsonArray,scheduleJsonArray,timeJsonArray){


  $(jQuery.parseJSON(JSON.stringify(sectionJsonArray))).each(function() {  
           var id = this.id;
           var level = this.level;
            var name = this.name;
            $("#MainDiv").append('<div class="div-table-row"  id="Section'+id+'"></div>');
            $("#Section"+id).append('<div class="div-table-col-sec">'+name+'('+level+')</div>');

            populateScheduleBySection(scheduleJsonArray, timeJsonArray, id)

  });
}

function populateSchedule(sectionJsonArray,scheduleJsonArray,timeJsonArray){


  $(jQuery.parseJSON(JSON.stringify(sectionJsonArray))).each(function() {  
           var id = this.id;
           var level = this.level;
            var name = this.name;
            $("#MainDiv").append('<div class="div-table-row"  id="Section'+id+'"></div>');
            $("#Section"+id).append('<div class="div-table-col-sec">'+name+'</div>');

            populateScheduleBySection(scheduleJsonArray, timeJsonArray, id)

  });
}

function populateRoomSchedule(roomJsonArray,scheduleJsonArray,timeJsonArray){


  $(jQuery.parseJSON(JSON.stringify(roomJsonArray))).each(function() {  
           var id = this.value;
            var name = this.text;
            $("#MainDiv").append('<div class="div-table-row"  id="Room'+id+'"></div>');
            $("#Room"+id).append('<div class="div-table-col-sec">'+name.substr(0,10)+'</div>');

            populateScheduleByRoom(scheduleJsonArray,timeJsonArray, id)
  });
}

function populateTeacherSchedule(teacherJsonArray,scheduleJsonArray,timeJsonArray){


  $(jQuery.parseJSON(JSON.stringify(teacherJsonArray))).each(function() {  
           var id = this.value;
            var fname = this.first_name;
            var lname = this.last_name;
            $("#MainDiv").append('<div class="div-table-row"  id="Teacher'+id+'"></div>');
            $("#Teacher"+id).append('<div class="div-table-col-sec">'+fname.substr(0,1)+'. '+lname+'</div>');

            populateScheduleByTeacher(scheduleJsonArray,timeJsonArray, id)

  });
}


function populateScheduleBySection(scheduleJsonArray,timeJsonArray, sectionId){
        var timeId;
        var timeDescription;
        var nextTimeId = null;


         $(jQuery.parseJSON(JSON.stringify(timeJsonArray))).each(function() {  
            timeId = this.id;
            timeDescription = this.time;

            if(timeId > 62 && timeId < 242)
            {

                  var timeEndCounter = getTimeByOrderNo(this.order_no+1, timeJsonArray);
                  // var condition = 0;
                  

                  if(nextTimeId==null){
                      nextTimeId = timeId;
                  }

            
                  if(timeId <= nextTimeId){
                      $(jQuery.parseJSON(JSON.stringify(scheduleJsonArray))).each(function() {  
                            // var id = this.id;
                            // var timeStart = this.time_start;
                            // var timeEnd = this.time_end;
                            // var classs = this.class
                            // var scheduleSectionId = this.section_id


                            var schedule = this;
                            

                            if(timeId == schedule.time_start && sectionId == schedule.section_id){

                                var timeStart = getTimeById(schedule.time_start,timeJsonArray);
                                var timeEnd = getTimeById(schedule.time_end,timeJsonArray);
                                var colspan = timeEnd.order_no - timeStart.order_no;

                                if(colspan>1){
                                  $("#Section"+sectionId).append('<div class="div-table-colspan'+colspan+'" onclick="editSchedule('+schedule.class_id+','+schedule.subject_offered_id+','+sectionId+','+schedule.time_start+','+schedule.time_end+','+schedule.teacher_id+','+schedule.campus_id+','+schedule.room_id+','+schedule.building_id+','+schedule.schedule_type_id+','+schedule.capacity+')" data-id="'+schedule.id+'" data-section="'+sectionId+'" data-time_start="'+schedule.time_start+'" data-time_end="'+schedule.time_end+'">'+schedule.class+'</div>');  
                                }else
                                {
                                  $("#Section"+sectionId).append('<div class="div-table-col" onclick="editSchedule('+schedule.class_id+','+schedule.subject_offered_id+','+sectionId+','+schedule.time_start+','+schedule.time_end+','+schedule.teacher_id+','+schedule.campus_id+','+schedule.room_id+','+schedule.building_id+','+schedule.schedule_type_id+','+schedule.capacity+')" data-id="'+schedule.id+'" data-section="'+sectionId+'" data-time_start="'+schedule.time_start+'" data-time_end="'+schedule.time_end+'">'+schedule.class+'</div>')
                                }
                                nextTimeId = schedule.time_end;
                                // condition = schedule.time_start;

                            }
                            
                           
                      });
                            if(timeId == nextTimeId){

                            
                                nextTimeId = null;
                                if(timeEndCounter != null && timeEndCounter != undefined)
                                {
                                  $("#Section"+sectionId).append('<div class="div-table-col-blank" data-toggle="modal" data-target="#timeModal" data-section="'+sectionId+'" data-time="'+timeId+'"></div>');  
                                }
                            }

                  }
            }
            // else if(timeEndCounter != null && timeEndCounter != undefined){
            //    $("#Section"+sectionId).append('<div class="div-table-col-blank" data-toggle="modal" data-target="#timeModal" data-section="'+sectionId+'" data-time="'+timeId+'">no classs</div>');  
            // }

            // else{

            // }  



          });
    closeModal();
} 

function populateScheduleByRoom(scheduleJsonArray,timeJsonArray, roomId){
        var timeId;
        var timeDescription;
        var nextTimeId = null;


         $(jQuery.parseJSON(JSON.stringify(timeJsonArray))).each(function() {  
            timeId = this.id;
            timeDescription = this.time;

            if(timeId > 62 && timeId < 242)
            {

                    var timeEndCounter = getTimeByOrderNo(this.order_no+1, timeJsonArray);
                    // var condition = 0;
                    

                    if(nextTimeId==null){
                        nextTimeId = timeId;
                    }

                    if(timeId <= nextTimeId){
                        $(jQuery.parseJSON(JSON.stringify(scheduleJsonArray))).each(function() {  
                              // var id = this.id;
                              // var timeStart = this.time_start;
                              // var timeEnd = this.time_end;
                              // var classs = this.class
                              // var scheduleSectionId = this.section_id


                              var schedule = this;
                              

                              if(timeId == schedule.time_start && roomId == schedule.room_id){

                                  var timeStart = getTimeById(schedule.time_start,timeJsonArray);
                                  var timeEnd = getTimeById(schedule.time_end,timeJsonArray);
                                  var colspan = timeEnd.order_no - timeStart.order_no;

                                  if(colspan>1){
                                    $("#Room"+roomId).append('<div class="div-table-colspan'+colspan+'" data-id="'+schedule.id+'" data-room="'+roomId+'" data-time_start="'+schedule.time_start+'" data-time_end="'+schedule.time_end+'">'+schedule.class+'</div>');  
                                  }else
                                  {
                                    $("#Room"+roomId).append('<div class="div-table-col" data-id="'+schedule.id+'" data-room="'+roomId+'" data-time_start="'+schedule.time_start+'" data-time_end="'+schedule.time_end+'">'+schedule.class+'</div>')
                                  }
                                  nextTimeId = schedule.time_end;
                                  // condition = schedule.time_start;

                              }
                              
                             
                        });
                              if(timeId == nextTimeId){

                              
                                  nextTimeId = null;
                                  if(timeEndCounter != null && timeEndCounter != undefined)
                                  {
                                    $("#Room"+roomId).append('<div class="div-table-col-blank" data-toggle="modal" data-target="#timeModal" data-room="'+roomId+'" data-time="'+timeId+'"></div>');  
                                  }
                              }

                    }
            }
            // else if(timeEndCounter != null && timeEndCounter != undefined){
            //    $("#Section"+sectionId).append('<div class="div-table-col-blank" data-toggle="modal" data-target="#timeModal" data-section="'+sectionId+'" data-time="'+timeId+'">no classs</div>');  
            // }

            // else{

            // }  



          });
} 


function populateScheduleByTeacher(scheduleJsonArray,timeJsonArray, teacherId){
        var timeId;
        var timeDescription;
        var nextTimeId = null;


         $(jQuery.parseJSON(JSON.stringify(timeJsonArray))).each(function() {  
            timeId = this.id;
            timeDescription = this.time;

            if(timeId > 62 && timeId < 242)
            {

            
                    var timeEndCounter = getTimeByOrderNo(this.order_no+1, timeJsonArray);
                    // var condition = 0;
                    

                    if(nextTimeId==null){
                        nextTimeId = timeId;
                    }

                    if(timeId <= nextTimeId){
                        $(jQuery.parseJSON(JSON.stringify(scheduleJsonArray))).each(function() {  
                              // var id = this.id;
                              // var timeStart = this.time_start;
                              // var timeEnd = this.time_end;
                              // var classs = this.class
                              // var scheduleSectionId = this.section_id


                              var schedule = this;
                              

                              if(timeId == schedule.time_start && teacherId == schedule.teacher_id){

                                  var timeStart = getTimeById(schedule.time_start,timeJsonArray);
                                  var timeEnd = getTimeById(schedule.time_end,timeJsonArray);
                                  var colspan = timeEnd.order_no - timeStart.order_no;

                                  if(colspan>1){
                                    $("#Teacher"+teacherId).append('<div class="div-table-colspan'+colspan+'" data-id="'+schedule.id+'" data-teacher="'+teacherId+'" data-time_start="'+schedule.time_start+'" data-time_end="'+schedule.time_end+'">'+schedule.class+'</div>');  
                                  }else
                                  {
                                    $("#Teacher"+teacherId).append('<div class="div-table-col" data-id="'+schedule.id+'" data-teacher="'+teacherId+'" data-time_start="'+schedule.time_start+'" data-time_end="'+schedule.time_end+'">'+schedule.class+'</div>')
                                  }
                                  nextTimeId = schedule.time_end;
                                  // condition = schedule.time_start;

                              }
                              
                             
                        });
                              if(timeId == nextTimeId){

                              
                                  nextTimeId = null;
                                  if(timeEndCounter != null && timeEndCounter != undefined)
                                  {
                                    $("#Teacher"+teacherId).append('<div class="div-table-col-blank" data-toggle="modal" data-target="#timeModal" data-teacher="'+teacherId+'" data-time="'+timeId+'"></div>');  
                                  }
                              }

                    }
            }
            // else if(timeEndCounter != null && timeEndCounter != undefined){
            //    $("#Section"+sectionId).append('<div class="div-table-col-blank" data-toggle="modal" data-target="#timeModal" data-section="'+sectionId+'" data-time="'+timeId+'">no classs</div>');  
            // }

            // else{

            // }  



          });
} 