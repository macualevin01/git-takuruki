<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventsMainController extends Controller
{
    public function index(){
        return view('events.index');
    }
}
