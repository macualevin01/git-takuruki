<?php

namespace App\Http\Controllers;
use App\Models\Cards;
use Illuminate\Http\Request;
date_default_timezone_set('Asia/Manila');
class CardMainConteroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        return view('card.index');
    }

    public function create(){
        return view('card/create');
    }

    public function store(Request $request)
    {
        if($request->only('card_name', 'description'))
        {
            $post_card = new Cards([
              'card_name' => $request->get('card_name'),
              'description' => $request->get('description'),
              'created_by_id' => \Auth::user()->id,
            ]);
            $post_card->save();
            if($post_card)
            {
                return redirect('card/create')->withSuccess('success');
            }
            else
            {
                return redirect('card/create')->withSuccess('errors');
            }
        }
        else
        {
            return redirect('card/create')->withSuccess('errors');
        }
    }
    public function data(){

        $card_list  = Cards::where('cards.created_by_id',\Auth::user()->id)->get();
        $card_arr = [];
        foreach($card_list as $card)
        {
            $action = '<a id="edit_btn" data-id="'.$card->id.'" ><i class="fa fa-edit text-success"> </i> </a> &nbsp;
                        <a id="edit_btn" data-id="'.$card->id.'" > <i class="fa fa-trash text-danger"> </i></a>';
            array_push($card_arr,['actions'=>$action,'card_name'=>$card->card_name,'description'=>$card->description,'created_at'=>date('F, d Y h:i A',strtotime($card->created_at))]);
        }
        $data['data'] = $card_arr;
        return response()->json($data);

    }
}
